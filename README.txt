iBoxplorer is covered under the ISC license as of July, 2013

/* 
 * Copyright (c) 2013, Charlie Reiman <tallest_head@nanomoai.com>
 * 
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

To build iBoxplorer, you will need to checkout two additional projects
as siblings to iboxplorer.

FineSlider:

git clone https://bitbucket.org/chairface/fine_slider FineSlider

And my fork of meager's color picker:

git clone https://bitbucket.org/chairface/meagar-color-picker-fork

These need to be adjacent to iboxplorer as it references those files
by source. You should then be able to just build iboxplorer in
XCode. The sibling projects don't need to be built.

iboxplorer is ARC compliant and builds under XCode 4.X with iOS
deployment target of iOS 5.0. Anything earlier than 5.0 won't support
ARC. Performance under the simulator is horrible so you'll really want
an iPad to do any development.

Charlie Reiman.
