/* 
 * Copyright (c) 2013, Charlie Reiman <tallest_head@nanomoai.com>
 * 
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

//  Created by Charlie Reiman on 4/10/13.

#ifndef iBoxplorer_AppEvents_h
#define iBoxplorer_AppEvents_h

#define APP_BASE_URL @"com.nanomoai.iboxplorer."

// ResetConfig is the signal to reset the model to the defaults.
#define kAppEventResetConfig    (APP_BASE_URL @"reset_config")
// ResetSync is the signal to update the views after the model was reset.
#define kAppEventResetSync      (APP_BASE_URL @"reset_sync")
// SaveConfig signals that it is time to write out the config to the user.cfg file. This is sent when backgrounding or when exiting the explore mode.
#define kAppEventSaveConfig     (APP_BASE_URL @"save_config")

// Capture captures the current image to the photo roll.
#define kAppEventCapture        (APP_BASE_URL @"capture")
// EditParameters is sent when the user clicks "more" from explore mode.
#define kAppEventEditParameters (APP_BASE_URL @"paramedit")
// ParameterDone is for when parameter editing is done.
#define kAppEventParameterDone  (APP_BASE_URL @"paramdone")
// ExploreDone returns the app to gallery mode.
#define kAppEventExploreDone    (APP_BASE_URL @"exploredone")

#define kAppEventDoAbout        (APP_BASE_URL @"about")
#define kAppEventDoHelp         (APP_BASE_URL @"help")

#endif

