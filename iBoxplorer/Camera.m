/* 
 * Copyright (c) 2013, Charlie Reiman <tallest_head@nanomoai.com>
 * 
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

//  Camera.m

//  Created by Charlie Reiman on 3/29/13.

#import "Camera.h"
#import <Accelerate/Accelerate.h>
#import "CfgValue.h"

@interface Camera ()
{
    GLfloat _camera[16];
}
@end

#define rightDirection (&_camera[0])
#define upDirection (&_camera[4])
#define direction (&_camera[8])
#define position (&_camera[12])

// DSP use for tiny vectors is questionable but I'd like to give it a go.
// normalize a 3 part vector to be 1 unit long. Returns 1 if the successful.
// Should only fail on zero vectors. Results is returned in place
static BOOL normalize(float *vec) {
    float dotp[3];
    float divisor[3];
    const int three=3;
    vDSP_dotpr(vec, 1, vec, 1, dotp, 3); // only one value for result. Don't be fooled.
    if (dotp[0] == 1.0) {
        return YES;
    }
    if (dotp[0] != 0.0) {
        dotp[1]=dotp[2]=dotp[0];
        vvrsqrtf(divisor, dotp, &three);
        vDSP_vmul(vec, 1, divisor, 1, vec, 1, 3);  // inplace result is ok
        return YES;
    }
    return NO;
}



@implementation Camera

- (Camera*) init
{
    self = [super init];
    if (self != nil) {
        _camera[0] = 1.0;
        _camera[5] = 1.0;  // (0,1,0) up
        _camera[10] = 1.0;  // (0,0,1) fwd (direction)
        _camera[12] = -0.464236; // position
        _camera[13] = 0.5914196;
        _camera[14] = 2.472434;
        _camera[15] = 1.0;
        [self normalizeCamera];
    }
    return self;
}

- (void) loadConfig:(Config*)config
{
    BPvec3 handyVector;
    handyVector = [config valueForKey:@"position"].vec3Value;
    position[0] = handyVector.x;
    position[1] = handyVector.y;
    position[2] = handyVector.z;
    handyVector = [config valueForKey:@"direction"].vec3Value;
    direction[0] = handyVector.x;
    direction[1] = handyVector.y;
    direction[2] = handyVector.z;
    handyVector = [config valueForKey:@"upDirection"].vec3Value;
    upDirection[0] = handyVector.x;
    upDirection[1] = handyVector.y;
    upDirection[2] = handyVector.z;
    [self normalizeCamera];
}

// Insures the camera vectors are all of 1 length. Only fwd and up
// must be normalized, then the rightVector is derived from those two.
-(void) normalizeCamera
{
    int i; float l;

    if (!normalize(direction)) {
        direction[0]=direction[1]=0.0;
        direction[2]=1.0;
    }

    // Orthogonalize and normalize upDirection.
    vDSP_dotpr(direction, 1, upDirection, 1, &l, 3);
    for (i=0; i<3; i++) upDirection[i] -= l*direction[i];
    
    if (!normalize(upDirection)) {  // Error? Make upDirection.z = 0.
        upDirection[2] = 0;
        if (fabs(direction[2]) == 1.0) { upDirection[0] = 0; upDirection[1] = 1; }
        else {
            upDirection[0] = -direction[1]; upDirection[1] = direction[0];
            normalize(upDirection);
        }
    }

    // Compute rightDirection as a cross product of upDirection and direction.
    for (i=0; i<3; i++) {
        int j = (i+1)%3, k = (i+2)%3;
        rightDirection[i] = upDirection[j]*direction[k] - upDirection[k]*direction[j];
    }
}

// Move camera in a direction relative to the view direction.
// Behaves like `glTranslate`.
- (void) moveCameraX:(float)x Y:(float)y Z:(float)z
{
    int i; for (i=0; i<3; i++) {
        position[i] += rightDirection[i]*x + upDirection[i]*y + direction[i]*z;
    }
}

// Check! I think this function is unused.
// Move camera in the normalized absolute direction `dir` by `len` units.
- (void) moveCameraAbsoluteDir:(float*)dir Len:(float)len
{
    int i; for (i=0; i<3; i++) {
        position[i] += len * dir[i];
    }
}

// Rotate the camera by `angle` radians around a normalized axis.
// Behaves like `glRotate` without normalizing the axis.
- (void) rotateCamera:(float)angle X:(float)x Y:(float)y Z:(float)z
{
    int i, j;
    float s = sin(angle), c = cos(angle);
    float t = 1.0-c;
    float r[3][3] = {
        { x*x*t +   c, x*y*t + z*s, x*z*t - y*s },
        { y*x*t - z*s, y*y*t +   c, y*z*t + x*s },
        { z*x*t + y*s, z*y*t - x*s, z*z*t +   c }
    };
    for (i=0; i<3; i++) {
        float c[3];
        for (j=0; j<3; j++) { c[j] = _camera[i+j*4]; }
        for (j=0; j<3; j++) { vDSP_dotpr(c, 1, r[j], 1, &(_camera[i+j*4]), 3); }
    }
}

- (GLfloat *) cameraUniform
{
    return _camera;
}

@end

