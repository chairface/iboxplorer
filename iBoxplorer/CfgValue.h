/* 
 * Copyright (c) 2013, Charlie Reiman <tallest_head@nanomoai.com>
 * 
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
//  CfgValue.h
//  Created by Charlie Reiman on 3/27/13.

#import <Foundation/Foundation.h>

// This represents values for user editable parameters. There's a lot
// of possible ways to do this so I'm just picking this sort of blobbish
// class to do it. Since "type" may be a reserved word in some situations,
// I'm using "vtype" wherever it appears naked.

typedef struct BPvec3
{
    float x, y, z;
} BPvec3;

@interface CfgValue : NSObject

+ (NSString*) typeFloat;
+ (NSString*) typeInt;
+ (NSString*) typeColor;
+ (NSString*) typeVec3;

+ (CfgValue*) cfgIntValueName:(NSString*)name value:(NSInteger)value min:(NSInteger)min max:(NSInteger)max;
+ (CfgValue*) cfgFloatValueName:(NSString*)name value:(float)value min:(float)min max:(float)max;
+ (CfgValue*) cfgColorValueName:(NSString*)name red:(float)red green:(float)green blue:(float)blue;
+ (CfgValue*) cfgVec3ValueName:(NSString*)name x:(float)x y:(float)y z:(float)z;
+ (CfgValue*) cfgValueFromString:(NSString*)inputLine;

// The values...
@property (nonatomic) NSInteger intValue;
@property (nonatomic) NSInteger intMaxValue;
@property (nonatomic) NSInteger intMinValue;

@property (nonatomic) float floatValue;
@property (nonatomic) float floatMaxValue;
@property (nonatomic) float floatMinValue;

@property (nonatomic, strong) UIColor *colorValue;
@property (nonatomic, readonly) GLfloat *colorValues;

@property (nonatomic, readonly) BPvec3 vec3Value;
- (void) setVec3Value:(BPvec3)vec3Value;

// ... and the metadata
@property (nonatomic, strong) NSString *name;
@property (nonatomic, readonly) NSString *vtype;
@property (nonatomic, readonly) BOOL hidden;

- (NSString*) cfgString;  // Return a config file string. Includes the newline.
- (const char *) cName;

@end
