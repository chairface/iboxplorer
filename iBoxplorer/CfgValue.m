/* 
 * Copyright (c) 2013, Charlie Reiman <tallest_head@nanomoai.com>
 * 
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
//  CfgValue.m
//  Created by Charlie Reiman on 3/27/13.

#import "CfgValue.h"

static GLfloat gColorBuf[3];  // XXX Dangerous but handy

@implementation CfgValue

+ (NSString*) typeFloat
{
    return @"float";
}

+ (NSString*) typeInt
{
    return @"int";
}

+ (NSString*) typeColor
{
    return @"color";
}

+ (NSString*) typeVec3
{
    return @"vec3";
}

+ (CfgValue*) cfgIntValueName:(NSString*)name value:(NSInteger)value min:(NSInteger)min max:(NSInteger)max
{
    CfgValue *result = [CfgValue alloc];
    result->_intValue = value;
    result->_intMaxValue = max;
    result->_intMinValue = min;
    result->_name = name;
    result->_vtype = [CfgValue typeInt];
    return result;
}

+ (CfgValue*) cfgFloatValueName:(NSString*)name value:(float)value min:(float)min max:(float)max
{
    CfgValue *result = [CfgValue alloc];
    result->_floatValue = value;
    result->_floatMaxValue = max;
    result->_floatMinValue = min;
    result->_name = name;
    result->_vtype = [CfgValue typeFloat];
    return result;
}

+ (CfgValue*) cfgColorValueName:(NSString*)name red:(float)red green:(float)green blue:(float)blue
{
    CfgValue *result = [CfgValue alloc];
    result->_colorValue = [UIColor colorWithRed:red green:green blue:blue alpha:1.0];
    result->_name = name;
    result->_vtype = [CfgValue typeColor];
    return result;
}

+ (CfgValue*) cfgVec3ValueName:(NSString*)name x:(float)x y:(float)y z:(float)z
{
    CfgValue *result = [CfgValue alloc];
    result->_vec3Value.x = x;
    result->_vec3Value.y = y;
    result->_vec3Value.z = z;
    result->_name = name;
    result->_vtype = [CfgValue typeVec3];
    return result;
}

+ (CfgValue*) cfgValueFromString:(NSString*)inputLine
{
    NSArray *inputWords = [inputLine componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSMutableArray *interestingWords = [[NSMutableArray alloc] initWithCapacity:[inputWords count]];
    CfgValue *result;
    BOOL hidden = NO;
    
    // Strip out the empty strings.
    for (NSString *word in inputWords) {
        if ([word length] != 0) {
            [interestingWords addObject:word];
        }
    }
    inputWords = nil; // Drop it just to be nice
    
    // Comment parsing probably doesn't need to be here.
    if ([interestingWords count] == 0 || [interestingWords[0] isEqualToString:@"#"]) {
        // NSLog(@"comment or blank");
        return nil;
    }

    // Catch hidden vars
    if ([interestingWords[0] hasPrefix:@"@"]) {
        hidden = YES;
        interestingWords[0] = [interestingWords[0] substringFromIndex:1];
    }
    // Luckily, all my types require five input fields.
    if ([interestingWords count] != 5) {
        NSLog(@"Unable to parse input \"%@\"", inputLine);
        return nil;
    }
    if ([interestingWords[1] isEqualToString:[CfgValue typeFloat]]) {
        result = [CfgValue cfgFloatValueName:interestingWords[0]
                                       value:[interestingWords[2] floatValue]
                                         min:[interestingWords[3] floatValue]
                                         max:[interestingWords[4] floatValue]];
    }
    else if ([interestingWords[1] isEqualToString:[CfgValue typeInt]]) {
        result = [CfgValue cfgIntValueName:interestingWords[0]
                                     value:[interestingWords[2] intValue]
                                       min:[interestingWords[3] intValue]
                                       max:[interestingWords[4] intValue]];
    }
    else if ([interestingWords[1] isEqualToString:[CfgValue typeColor]]) {
        result = [CfgValue cfgColorValueName:interestingWords[0]
                                         red:[interestingWords[2] floatValue]
                                       green:[interestingWords[3] floatValue]
                                        blue:[interestingWords[4] floatValue]];
    }
    else if ([interestingWords[1] isEqualToString:[CfgValue typeVec3]]) {
        result = [CfgValue cfgVec3ValueName:interestingWords[0]
                                          x:[interestingWords[2] floatValue]
                                          y:[interestingWords[3] floatValue]
                                          z:[interestingWords[4] floatValue]];
    }
    else {
        NSLog(@"Type of %@ unrecognized", interestingWords[1]);
        return nil;
    }
    result->_hidden = hidden;
    return result;
}

// Return a config file string including the trailing newline.
// The format is simple whitespace separated strings:
// <name> <type> <value> <min> <max>
- (NSString*) cfgString
{
    NSString *hidden = @"";
    if (self.hidden) {
        hidden = @"@";
    }
    if ([self.vtype isEqualToString:[CfgValue typeFloat]]) {
        return [NSString stringWithFormat:@"%@%@ %@ %g %g %g", hidden, self.name, self.vtype,
                self.floatValue, self.floatMinValue, self.floatMaxValue];
    }
    else if ([self.vtype isEqualToString:[CfgValue typeInt]]) {
        return [NSString stringWithFormat:@"%@%@ %@ %d %d %d", hidden, self.name, self.vtype,
                self.intValue, self.intMinValue, self.intMaxValue];
    }
    else if ([self.vtype isEqualToString:[CfgValue typeColor]]) {
        CGFloat red, green, blue, alpha;
        [self.colorValue getRed:&red green:&green blue:&blue alpha:&alpha];
        return [NSString stringWithFormat:@"%@%@ %@ %g %g %g", hidden, self.name, self.vtype,
                red, green, blue];
    }
    else if ([self.vtype isEqualToString:[CfgValue typeVec3]]) {
        return [NSString stringWithFormat:@"%@%@ %@ %g %g %g", hidden, self.name, self.vtype,
                _vec3Value.x, _vec3Value.y, _vec3Value.z];
    }
    return @"# Unknown value type\n";
}

- (const char *) cName
{
    return [self.name cStringUsingEncoding:NSASCIIStringEncoding];
}

- (void) setVec3Value:(BPvec3)vec3Value
{
    _vec3Value.x = vec3Value.x;
    _vec3Value.y = vec3Value.y;
    _vec3Value.z = vec3Value.z;
}

// This is for GL which wants the colors in a tiny array

- (GLfloat*) colorValues
{
    CGFloat red, green, blue, alpha;
    [self.colorValue getRed:&red green:&green blue:&blue alpha:&alpha];
    gColorBuf[0] = red;
    gColorBuf[1] = green;
    gColorBuf[2] = blue;
    return gColorBuf;
}

@end
