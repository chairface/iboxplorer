/* 
 * Copyright (c) 2013, Charlie Reiman <tallest_head@nanomoai.com>
 * 
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
//
//  ColorView.m
//
//  Created by Charlie Reiman on 4/6/13.
//

#import <QuartzCore/QuartzCore.h>
#import "ColorView.h"
#import "DrawDriver.h"

@implementation ColorView
{
    UIView                 *_swatch;
    UITapGestureRecognizer *_tap;
    UIPopoverController    *_popover;
}

- (id) initWithFrame:(CGRect)rowFrame color:(UIColor*)rowColor value:(CfgValue*)cVal
{
    self = [super initWithFrame:rowFrame];
    if (self) {
        float rowHeight = rowFrame.size.height;
        float width = rowFrame.size.width;

        self.backgroundColor = rowColor;

        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(4, 0, (width/2)-4, rowHeight/3)];
        label.text = cVal.name;
        label.backgroundColor = [UIColor clearColor];
        [self addSubview:label];

        CGRect swatchRect = CGRectMake(width-100, rowHeight/5, 80, 3*rowHeight/5);
        _swatch = [[UIView alloc] initWithFrame:swatchRect];
        _swatch.backgroundColor = cVal.colorValue;
        [self addSubview:_swatch];
        _swatch.layer.shadowColor = [UIColor blackColor].CGColor;
        _swatch.layer.shadowOpacity = 0.93;
        _swatch.layer.shadowRadius = 4.0;
        UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:_swatch.bounds];
        _swatch.layer.shadowPath = shadowPath.CGPath;

        _tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapped:)];
        [_swatch addGestureRecognizer:_tap];

        self.cVal = cVal;
    }
    return self;
}

- (void) colorPickerSaved:(ColorPickerController *)controller parameter:(id)parameter
{
}

- (void) colorPickerCancelled:(ColorPickerController *)controller parameter:(id)parameter
{
}

- (void) colorPickerValueChanged:(ColorPickerController *)controller parameter:(id) parameter
{
    [self.cVal setColorValue:[controller selectedColor]];
    _swatch.layer.backgroundColor = [controller selectedColor].CGColor;
    [[DrawDriver instance] setCtlChange];
}


- (void) tapped:(UITapGestureRecognizer*)gesture
{
    ColorPickerController *colorPicker;
    
    colorPicker = [[ColorPickerController alloc] initWithColor:self.cVal.colorValue
                                                      andTitle:@"Pick a color"];
    _popover = [[UIPopoverController alloc] initWithContentViewController:colorPicker];
    _popover.popoverContentSize = CGSizeMake(300, 400);

    colorPicker.delegate = self;

    [_popover presentPopoverFromRect:_swatch.frame
                              inView:self
            permittedArrowDirections:UIPopoverArrowDirectionRight | UIPopoverArrowDirectionLeft
                            animated:YES];
}

- (void) setCVal:(CfgValue *)cVal
{
    [super setCVal:cVal];
    _swatch.backgroundColor = cVal.colorValue;
}



@end
