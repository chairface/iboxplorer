/* 
 * Copyright (c) 2013, Charlie Reiman <tallest_head@nanomoai.com>
 * 
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
//  Config.m
//
//  Created by Charlie Reiman on 3/27/13.
//

#import "Config.h"
#import "CfgValue.h"
#import "AppDelegate.h"

@interface Config ()
{
    NSString *_defaultPath, *_userPath;
    NSDictionary *_configDict;
    NSArray *_valuesInOrder;
}
- (NSArray*) loadValues:(NSString*)path;
- (NSDictionary*) createConfigDict;

@end

@implementation Config

static Config *staticGlobalConfig;

- (id) initWithDefault:(NSString*)dPath User:(NSString*)uPath
{
    self = [super init];
    if (self != nil) {
        NSString *resourcePath = [[NSBundle mainBundle] resourcePath];
        _defaultPath = [resourcePath stringByAppendingPathComponent:dPath];

        NSArray  *documentPathList = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory,
                                                                         NSUserDomainMask,
                                                                         YES);

        NSString *documentPath = documentPathList[0];
        _userPath = [documentPath stringByAppendingPathComponent:uPath];
        
        _valuesInOrder = [self loadValues:_userPath];
        if (_valuesInOrder == nil) {
            _valuesInOrder = [self loadValues:_defaultPath];
        }
        _configDict = [self createConfigDict];
    }
    return self;
}

- (NSArray*) loadValues:(NSString *)path
{
    NSError *error;
    NSString *fileContents = [NSString stringWithContentsOfFile:path encoding:NSASCIIStringEncoding error:&error];
    if (fileContents==nil || [fileContents isEqualToString:@""]) {
        return nil;
    }
    // Parse each line
    NSArray *lines = [fileContents componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    NSMutableArray *configs = [[NSMutableArray alloc] init];
    for (NSString *line in lines)
    {
        CfgValue *cfgValue = [CfgValue cfgValueFromString:line];
        if (cfgValue != nil) {
            [configs addObject:cfgValue];
        }
    }
    return configs;
}

- (NSDictionary*) createConfigDict
{
    NSMutableDictionary *result = [[NSMutableDictionary alloc] initWithCapacity:[_valuesInOrder count]];
    for (CfgValue *cfgValue in _valuesInOrder) {
        [result setValue:cfgValue forKey:cfgValue.name];
    }
    return result;
}

- (void) writeToUserPath
{
#if 0
    // Doesn't for for some mysterious reason
    NSOutputStream *output = [NSOutputStream outputStreamToFileAtPath:_userPath append:NO];
    for (CfgValue *cval in _valuesInOrder) {
        const char *bytes;
        NSUInteger len;
        bytes = [[cval cfgString] cStringUsingEncoding:NSASCIIStringEncoding];
        len = strlen(bytes); // sigh.
        [output write:(uint8_t*)bytes maxLength:len];
    }
    [output close];
#else 
    NSMutableData *data = [[NSMutableData alloc] init];
    for (CfgValue *cval in _valuesInOrder) {
        const char *nl = "\n";
        const char *bytes;
        NSUInteger len;
        bytes = [[cval cfgString] cStringUsingEncoding:NSASCIIStringEncoding];
        len = strlen(bytes); // sigh.
        [data appendBytes:bytes length:len];
        [data appendBytes:nl length:1];
    }
    [[NSFileManager defaultManager] createFileAtPath:_userPath contents:data attributes:nil];
#endif
}

- (void) deleteUserData
{
    NSError *error;
    [[NSFileManager defaultManager] removeItemAtPath:_userPath error:&error];
}

- (CfgValue*) valueForKey:(NSString*)key
{
    return _configDict[key];
}

+ (Config*) globalConfig
{
    return staticGlobalConfig;
}

+ (void) setGlobalConfig:(Config *)config
{
    staticGlobalConfig = config;
}

- (NSArray*) values
{
    return _valuesInOrder;
}

@end
