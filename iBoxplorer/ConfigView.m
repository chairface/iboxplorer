/* 
 * Copyright (c) 2013, Charlie Reiman <tallest_head@nanomoai.com>
 * 
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
//  ConfigView.m

//  Created by Charlie Reiman on 4/5/13.
//

#import "ConfigView.h"
#import "Config.h"
#import "CfgValue.h"

#import "FloatView.h"
#import "IntView.h"
#import "ColorView.h"

#import "AppEvents.h"

@implementation ConfigView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self loadup];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(syncAfterReset:) name:kAppEventResetSync object:nil];
    }
    return self;
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kAppEventResetSync object:nil];
}

- (void) loadup
{
    NSArray *confValues = [Config globalConfig].values;
    float width = self.bounds.size.width;
    float top = 0;
    const float rowHeight = 80;
    BOOL  rowStripe = NO;

    for (CfgValue *cVal in confValues) {
        if (!cVal.hidden) {
            CGRect rowFrame = CGRectMake(0, top, width, rowHeight);
            UIColor *rowColor;

            rowStripe = !rowStripe;
            rowColor = (rowStripe ? [UIColor colorWithRed:0.90 green:0.90 blue:0.90 alpha:1.0]:
                        [UIColor whiteColor]);

            if (cVal.vtype == [CfgValue typeFloat]) {
                [self addSubview:[[FloatView alloc] initWithFrame:rowFrame color:rowColor value:cVal]];
            }
            else if (cVal.vtype == [CfgValue typeInt]) {
                [self addSubview:[[IntView alloc] initWithFrame:rowFrame color:rowColor value:cVal]];
            }
            else if (cVal.vtype == [CfgValue typeColor]) {
                [self addSubview:[[ColorView alloc] initWithFrame:rowFrame color:rowColor value:cVal]];
            }
            top += rowHeight;
        }
    }
    CGRect newBounds = CGRectMake(0, 0, width, top);
    self.frame = newBounds;
}

- (void) syncAfterReset:(NSNotification*)notice
{
    NSArray *confValues = [Config globalConfig].values;
    NSMutableArray *subviews = [[NSMutableArray alloc] init];
    [subviews addObjectsFromArray:self.subviews];
    
    for (CfgValue *cVal in confValues) {
        if (!cVal.hidden) {
            ConfigValueView *view = subviews[0];
            [subviews removeObjectAtIndex:0];
            view.cVal = cVal;
        }
    }
}

@end
