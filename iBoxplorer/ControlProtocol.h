/* 
 * Copyright (c) 2013, Charlie Reiman <tallest_head@nanomoai.com>
 * 
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
//  TranslateProtocol.h
//
//  Created by Charlie Reiman on 3/26/13.
//

#import <Foundation/Foundation.h>

// This protocol is more of a delegate and lets me isolate the
// control input view from the controlled view.
@protocol ControlProtocol <NSObject>

- (void) translateX:(float)x Y:(float)y Z:(float)z;
- (void) orientX:(float)x Y:(float)y Z:(float)z;
- (void) setWidth:(float)x height:(float)y;
- (void) setFOVWidth:(float)angle;
- (void) setFOVHeight:(float)angle;

@end
