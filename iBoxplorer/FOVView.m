/* 
 * Copyright (c) 2013, Charlie Reiman <tallest_head@nanomoai.com>
 * 
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
//  FOVView.m
//
//  Created by Charlie Reiman on 3/27/13.
//

#import "FOVView.h"
#import "DrawDriver.h"
#import <QuartzCore/QuartzCore.h>

#define kLowAngle 0.34906585039889  // 20 degrees
#define kHighAngle 2.08282385873355 // 120 degrees
#define kAngleSpread (kHighAngle - kLowAngle)

#define RAD2DEG (180.0/M_PI)

@interface FOVView ()

@property (nonatomic, strong) CALayer *cursorLayer;

@end

@implementation FOVView

- (void)layoutSublayersOfLayer:(CALayer *)layer
{
    // Invert the Y axis so the controls map more naturally to the coordinate space.
    CATransform3D transform = CATransform3DScale(CATransform3DIdentity, 1.0, -1.0, 1.0);
    layer.transform = transform;
    
    // Let's add a gradient.
    CFMutableArrayRef colors = CFArrayCreateMutable(nil , 3, nil);
    CFArraySetValueAtIndex(colors, 0, [UIColor lightGrayColor].CGColor);
    CFArraySetValueAtIndex(colors, 1, [UIColor darkGrayColor].CGColor);
    CFArraySetValueAtIndex(colors, 2, [UIColor lightGrayColor].CGColor);
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.bounds = layer.bounds;
    gradient.colors = (__bridge NSArray*)colors;
    gradient.anchorPoint = CGPointZero;
    gradient.startPoint = CGPointMake(0.0, 1.0);
    gradient.endPoint = CGPointMake(1.0, 0.0);
    [layer addSublayer:gradient];
    CFRelease(colors);
    
    // And the diagonal
    CGMutablePathRef diagonal = CGPathCreateMutable();
    CGPathMoveToPoint(diagonal, nil, 0.0, 0.0);
    CGPathAddLineToPoint(diagonal, nil, layer.bounds.size.width, layer.bounds.size.height);
    CAShapeLayer *diagonalLayer = [CAShapeLayer layer];
    diagonalLayer.path = diagonal;
    diagonalLayer.strokeColor = [UIColor whiteColor].CGColor;
    diagonalLayer.lineWidth = 2.0;
    [layer addSublayer:diagonalLayer];
    CGPathRelease(diagonal);
    
    // Let's outline this control in white
    CGRect outline = CGRectMake(-0.5f, -0.5f, layer.bounds.size.width+1, layer.bounds.size.height+1);
    CGMutablePathRef outlinePath = CGPathCreateMutable();
    CGPathAddRect(outlinePath, nil, outline);
    CAShapeLayer *outlineLayer = [CAShapeLayer layer];
    outlineLayer.path = outlinePath;
    outlineLayer.strokeColor = [UIColor whiteColor].CGColor;
    outlineLayer.fillColor = nil;
    outlineLayer.opacity = 1.0;
    outlineLayer.lineWidth = 1.0;
    [layer addSublayer:outlineLayer];
    CGPathRelease(outlinePath);
    
    // Last the cursor. It's a yellow circle around a black circle.
    self.cursorLayer = [CALayer layer];
    self.cursorLayer.bounds = CGRectMake(0.0, 0.0, 8.0, 8.0);
    CGMutablePathRef circle = CGPathCreateMutable();
    CGPathAddEllipseInRect(circle, nil, self.cursorLayer.bounds);
    CAShapeLayer *circleLayer = [CAShapeLayer layer];
    circleLayer.path = circle;
    circleLayer.strokeColor = [UIColor yellowColor].CGColor;
    circleLayer.fillColor = [UIColor clearColor].CGColor;
    circleLayer.lineWidth = 1.0;
    [self.cursorLayer addSublayer:circleLayer];
    CGPathRelease(circle);
    
    circle = CGPathCreateMutable();
    CGPathAddEllipseInRect(circle, nil, CGRectInset(self.cursorLayer.bounds, 1.0, 1.0));
    circleLayer = [CAShapeLayer layer];
    circleLayer.path = circle;
    circleLayer.strokeColor = [UIColor blackColor].CGColor;
    circleLayer.fillColor = [UIColor clearColor].CGColor;
    circleLayer.lineWidth = 1.0;
    [self.cursorLayer addSublayer:circleLayer];
    CGPathRelease(circle);
    
    [layer addSublayer:self.cursorLayer];

    self.fov = CGPointMake(M_PI_2, M_PI_2);
}

- (void) setFov:(CGPoint)fov
{
    if (fov.y > kHighAngle) fov.y = kHighAngle;
    if (fov.y < kLowAngle) fov.y = kLowAngle;
    if (fov.x > kHighAngle) fov.x = kHighAngle;
    if (fov.x < kLowAngle) fov.x = kLowAngle;
    if (fabs(fov.x-fov.y) < 0.1) {
        fov.x = (fov.x + fov.y) * 0.5;
        fov.y = fov.x;
    }
    _fov = fov;
    CGPoint cursorPos = self.cursorLayer.position;
    cursorPos.y = ((fov.y - kLowAngle)/kAngleSpread) * self.bounds.size.height;
    cursorPos.x = ((fov.x - kLowAngle)/kAngleSpread) * self.bounds.size.width;
    [CATransaction setDisableActions:YES];
    self.cursorLayer.position = cursorPos;
    [self.delegate fovView:self fov:_fov];
}

- (void) handleTouch:(UITouch*)touch
{
    CGPoint tPoint = [touch locationInView:self];
    CGPoint newFov;
    // Convert point to FOV values
    newFov.x = ((tPoint.x/self.bounds.size.width)*kAngleSpread) + kLowAngle;
    newFov.y = ((tPoint.y/self.bounds.size.height)*kAngleSpread) + kLowAngle;
    [self setFov:newFov];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    [self handleTouch:touch];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    [self handleTouch:touch];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
}


- (float) fovXdegrees
{
    return _fov.x * RAD2DEG;
}

- (float) fovYdegrees
{
    return _fov.y * RAD2DEG;
}

@end
