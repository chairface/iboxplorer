/* 
 * Copyright (c) 2013, Charlie Reiman <tallest_head@nanomoai.com>
 * 
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
//  FloatView.m
//
//  Created by Charlie Reiman on 4/6/13.
//

#import "FloatView.h"
#import "DrawDriver.h"
#import "FineSlider.h"

@implementation FloatView
{
    FineSlider *_slider;
    UILabel    *_valueLabel;
}

- (id) initWithFrame:(CGRect)rowFrame color:(UIColor*)rowColor value:(CfgValue*)cVal
{
    self = [super initWithFrame:rowFrame];
    if (self) {
        float rowHeight = rowFrame.size.height;
        float width = rowFrame.size.width;

        self.backgroundColor = rowColor;

        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(4, 0, (width/2)-4, rowHeight/3)];
        label.text = cVal.name;
        label.backgroundColor = [UIColor clearColor];
        [self addSubview:label];

        _slider = [[FineSlider alloc] initWithFrame:CGRectMake(4, rowHeight/3, width-8, rowHeight/3)];
        _slider.maximumValue = cVal.floatMaxValue;
        _slider.minimumValue = cVal.floatMinValue;
        _slider.value = cVal.floatValue;
        [_slider addTarget:self action:@selector(setFloatValue:) forControlEvents:UIControlEventValueChanged];
        [self addSubview:_slider];
        _valueLabel = [[UILabel alloc] initWithFrame:CGRectMake((width/2)+4, 0, (width/2)-8, rowHeight/3)];
        _valueLabel.text = [NSString stringWithFormat:@"%g", cVal.floatValue];
        _valueLabel.textAlignment = NSTextAlignmentRight;
        _valueLabel.backgroundColor = [UIColor clearColor];
        [self addSubview:_valueLabel];
        
        self.cVal = cVal;
    }
    return self;
}

- (void) setFloatValue:(UISlider*)control
{
    float oldVal = self.cVal.floatValue;
    float newVal = control.value;

    //control.value = newVal;

    if (oldVal != newVal) {
        _valueLabel.text = [NSString stringWithFormat:@"%g", newVal];
        self.cVal.floatValue = newVal;
        [[DrawDriver instance] setCtlChange];
    }
}

- (void) setCVal:(CfgValue *)cVal
{
    [super setCVal:cVal];
    _slider.value = cVal.floatValue;
    _valueLabel.text = [NSString stringWithFormat:@"%g", cVal.floatValue];
}

@end
