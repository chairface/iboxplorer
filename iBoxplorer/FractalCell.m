/* 
 * Copyright (c) 2013, Charlie Reiman <tallest_head@nanomoai.com>
 * 
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
//
//  FractalCell.m
//
//  Created by Charlie Reiman on 10/24/11.
//

#import "FractalCell.h"

// I don't know if I need to cache the random value as I am here or not. 
// It depends on the style of sort Apple uses and they aren't telling me.

@implementation FractalCell

- (FractalCell*) initWithCGRect: (CGRect*)rect
{
    if (self=[super init]) {
        dncRect = *rect;
        random = arc4random();  // initial sort will be randomized
    }
    return self;
}

- (NSInteger) randomCompare: (FractalCell*) other
{
    if (random < other->random) {
        return NSOrderedAscending;
    }
    else if (random > other->random) {
        return NSOrderedDescending;
    }
    return NSOrderedSame;
}

- (NSInteger) distCompare: (FractalCell*) other
{
    float d1 = CGRectGetMidX(dncRect);
    d1 *= d1;
    float scratch = CGRectGetMidY(dncRect);
    d1 += scratch * scratch;
    
    float d2 = CGRectGetMidX(other->dncRect);
    d2 *= d2;
    scratch = CGRectGetMidY(other->dncRect);
    d2 += scratch * scratch;
    
    
    if (d1 < d2) {
        return NSOrderedAscending;
    }
    else if (d1 > d2) {
        return NSOrderedDescending;
    }
    return NSOrderedSame;
}

- (CGRect) getRect
{
    return dncRect;
}


@end
