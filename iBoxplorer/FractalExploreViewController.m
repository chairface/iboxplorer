/* 
 * Copyright (c) 2013, Charlie Reiman <tallest_head@nanomoai.com>
 * 
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
//
//  FractalExploreViewController.m
//
//  Created by Charlie Reiman on 3/25/13.
//

#import "FractalExploreViewController.h"
#import "FractalGLView.h"
#import "OffscreenFractal.h"
#import "DrawDriver.h"
#import <QuartzCore/QuartzCore.h>

// This class is responsible for driving the off screen rendering
// via OffscreenFractal, handling gestures in the view, and
// blitting the offscreen fractal onscreen.
@interface FractalExploreViewController ()
{
    OffscreenFractal *_fractalRenderEngine;
    BOOL _lastWasZero; // to avoid passing endless zeros.
}

@property (readonly, strong) UIRotationGestureRecognizer *rotateGesture;
@property (readonly, strong) UIPanGestureRecognizer *panGesture;

- (void) presentTexture:(NSNotification*)notice;

- (IBAction) doRotate:(UIRotationGestureRecognizer*)rotater;
- (IBAction) doPan:(UIPanGestureRecognizer*)panner;
- (FractalGLView*) myView;
- (void) pollControls:(NSTimer*)timer;

@end

@implementation FractalExploreViewController

- (FractalExploreViewController*) initWithAccessor:(id<GUIStateAccess>)accessor
{
    if ((self=[super initWithAccessor:accessor]) != nil) {
        _rotateGesture = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(doRotate:)];
        _panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(doPan:)];
        _panGesture.maximumNumberOfTouches = 2;
        _panGesture.delegate = self;
        _rotateGesture.delegate = self;
        
        _fractalRenderEngine = [[OffscreenFractal alloc] init];
        _fractalRenderEngine.guiState = accessor;
        [_fractalRenderEngine readConfig];
        [_fractalRenderEngine createFramebuffer];
        [_fractalRenderEngine loadShaders];
        [_fractalRenderEngine resizeTexture:CGSizeMake(128, 128)]; // XXX Just for now
        [_fractalRenderEngine prepareQuad];
        [_fractalRenderEngine setFOVHeight:M_PI_2];
        [_fractalRenderEngine setFOVWidth:M_PI_2];
    }
    return self;
}

- (void) viewDidLoad
{
    [super viewDidLoad];

    NSAssert([self.view isKindOfClass:[FractalGLView class]], @"FractalExploreViewController requires a FractalGLView for its view");

    [self.view addGestureRecognizer:_rotateGesture];
    [self.view addGestureRecognizer:_panGesture];

    // Set up control polling
    self.controlPoller = [NSTimer scheduledTimerWithTimeInterval:1.0/20.0 target:self
                                                        selector:@selector(pollControls:)
                                                        userInfo:nil repeats:YES];
    // Register notice
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(presentTexture:) name:kNewFractalFrame object:nil];
    
    // Schedule a drawing ASAP
    [[DrawDriver instance] setTarget:_fractalRenderEngine];
    [[DrawDriver instance] setAction:@selector(drawTexture:)];
    [[DrawDriver instance] setCtlChange];  // Signal this draw as a control change.
}

- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[DrawDriver instance] setTarget: nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNewFractalFrame object:nil];
}

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)                                    gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
           shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

// This is for convenience/type saftey.
- (FractalGLView*) myView
{
    return (FractalGLView*)self.view;
}

- (void) presentTexture:(NSNotification*)notice
{
    BOOL isComplete = [[notice object] boolValue];
    
    [self.myView presentTexture:_fractalRenderEngine isComplete:isComplete];
}


- (id<ControlProtocol>) controllable
{
    return _fractalRenderEngine;
}

#pragma mark - User actions

// Controls roll
- (IBAction) doRotate:(UIRotationGestureRecognizer*)rotater
{
    // There are rotation and velocity properties in radians. You may assign 0 to the rotation but that will zero
    // the velocity.
    //[self orientX:0.0 Y:0.0 Z:rotater.rotation];
    [_fractalRenderEngine orientX:0.0 Y:0.0 Z:rotater.rotation];
    rotater.rotation = 0.0;
    [[DrawDriver instance] setCtlChange];
}

// Controls pitch and yaw
- (IBAction) doPan:(UIPanGestureRecognizer*)panner
{
    // This one is tricky as I need to wrangle a little math with the field of view
    // and the view bounds.
    CGPoint translate = [panner translationInView:self.view];
    [panner setTranslation:CGPointZero inView:self.view];
    CGPoint fov = [_fractalRenderEngine getFOV];

    translate.x /= self.view.bounds.size.width;
    translate.x *= -fov.x;
    translate.y /= self.view.bounds.size.height;
    translate.y *= -fov.y;
    [_fractalRenderEngine orientX:translate.x Y:translate.y Z:0];
    [[DrawDriver instance] setCtlChange];
}

#pragma mark - InfoExploreSomethingDelegate
- (void) infoExploreViewController:(InfoExploreViewController *)ievc setSize:(float)size
{
    [_fractalRenderEngine resizeTexture:CGSizeMake(size, size)];
}

- (void) infoExploreViewController:(InfoExploreViewController *)ievc setProgressive:(BOOL)prog
{
    [_fractalRenderEngine setProgressive:prog];
}

#pragma mark - Control polling
//
// The throttle and translation controls are velocities. So I need to poll
// them at regular intervals and can't use the demand driven system I use
// for FOV. To put it another way, I care if they have non zero values.
// For other controls, I care if the value changed and that mechanism
// works fine as an external system.

- (void) pollControls:(NSTimer *)timer
{
    CGFloat velX, velY, velZ;
    CGPoint lateralMotion = [self.translate motionVector];

    velX = lateralMotion.x;
    velY = lateralMotion.y;
    velZ = [self.throttle throttle];

    BOOL isZero = (velX == 0.0 && velY == 0.0 && velZ == 0.0);

    if  (!isZero || !_lastWasZero) {
        velX *= velX * velX; // cube it
        velY *= velY * velY;
        velZ *= velZ * velZ;
        
        velX *= self.granularity.cookedGranularity;
        velY *= self.granularity.cookedGranularity;
        velZ *= self.granularity.cookedGranularity;

        [_fractalRenderEngine translateX:velX Y:velY Z:velZ];
        [[DrawDriver instance] setCtlChange];
        _lastWasZero = isZero;
    }
}




@end

