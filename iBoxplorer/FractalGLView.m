/* 
 * Copyright (c) 2013, Charlie Reiman <tallest_head@nanomoai.com>
 * 
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
//
//  FractalGLView.m
//
//  Created by Charlie Reiman on 3/26/13.
//

#import "FractalGLView.h"
#import "GLARE.h"
#import "Shader.h"
#import "AppEvents.h"
#import <QuartzCore/QuartzCore.h>

static const GLfloat squareVertices[] = {
    -1.0f, -1.0f,
    1.0f,  -1.0f,
    -1.0f,  1.0f,
    1.0f,   1.0f,
};

static const GLushort squareElements[] = {
    0, 1, 2, 3
};


@interface FractalGLView ()
{
    GLuint _defaultFramebuffer;
    GLuint _colorRenderbuffer;
    
    // Quad geometery
    GLuint _vertex_bo;
    GLuint _vertex_name;
    GLuint _element_bo;

    GLuint _program;
    
    GLuint _fractal_uniform;

    BOOL   _wantCapture;
}

- (void) initVBO;
- (void) loadShader;
- (void) createFramebuffer;
- (void) deleteFramebuffer;
- (void) setContext:(EAGLContext*)newContest;
- (void) setWantCapture:(NSNotification*)notice;

@end

@implementation FractalGLView

+ (Class) layerClass
{
    return [CAEAGLLayer class];
}

- (id) initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        CAEAGLLayer *eaglLayer = (CAEAGLLayer *)self.layer;
        
        eaglLayer.opaque = YES;
        eaglLayer.drawableProperties = @{kEAGLDrawablePropertyRetainedBacking : @0,
                                         kEAGLDrawablePropertyColorFormat : kEAGLColorFormatRGBA8};
        EAGLContext *aContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
        if (!aContext) {
            NSLog(@"Failed to create ES context");
        }
        else {
            self.context = aContext;
        }
        [self initVBO];
        [self loadShader];
        [self setContentScaleFactor:2.0];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(setWantCapture:)
                                                     name:kAppEventCapture
                                                   object:nil];
        _wantCapture = NO;
    }
    return self;
}

// The vertex buffer objects for this view never change. All they are
// are the square coordinates & vertex ordering for the full screen
// quad drawing.
- (void) initVBO
{
    [self setContext];
    glGenBuffers(1, &_vertex_bo); GLARE;
    glBindBuffer(GL_ARRAY_BUFFER, _vertex_bo); GLARE;
    glBufferData(GL_ARRAY_BUFFER, sizeof(squareVertices), squareVertices, GL_STATIC_DRAW); GLARE;

    glGenBuffers(1, &_element_bo); GLARE;
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _element_bo); GLARE;
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(squareElements), squareElements, GL_STATIC_DRAW); GLARE;
}

- (void) loadShader
{
    Shader *shader = [[Shader alloc] initWithShadersInAppBundle:@"quad"];
    _program = [shader programObject];
    [shader validateProgram];
    _fractal_uniform = [shader getUniformLocation:"fractal_texture"];
    _vertex_name = [shader getAttributeLocation:"vertex"];
}

- (void) dealloc
{
    [self deleteFramebuffer];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kAppEventCapture object:nil];
}

- (void) setWantCapture:(NSNotification*)notice
{
    _wantCapture = YES;
}

// Assign a new context and make it active
- (void) setContext:(EAGLContext *)newContext
{
    if (_context != newContext) {
        [self deleteFramebuffer];
        
        _context = newContext;
        
        [EAGLContext setCurrentContext:_context];
    }
}

// Make the views context active.
- (BOOL) setContext
{
    if (self.context != nil) {
        [EAGLContext setCurrentContext:self.context];
        return YES;
    }
    return NO;
}

- (void) createFramebuffer
{
    if ([self setContext] && !_defaultFramebuffer) {
        // Create default buffer objects.
        glGenFramebuffers(1, &_defaultFramebuffer); GLARE;
        glBindFramebuffer(GL_FRAMEBUFFER, _defaultFramebuffer); GLARE;

        glGenRenderbuffers(1, &_colorRenderbuffer); GLARE;
        glBindRenderbuffer(GL_RENDERBUFFER, _colorRenderbuffer); GLARE;
        
        // bind them.
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, _colorRenderbuffer); GLARE;
        // Create color render buffer and allocate backing store.
        [self.context renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *)self.layer];
        
        if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
            NSLog(@"%s:%d failed to make complete framebuffer object 0x%x", __func__, __LINE__, glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }
}

- (void) deleteFramebuffer
{
    if ([self setContext]) {
        
        if (_defaultFramebuffer) {
            glDeleteFramebuffers(1, &_defaultFramebuffer);
            _defaultFramebuffer = 0;
        }
        
        if (_colorRenderbuffer) {
            glDeleteRenderbuffers(1, &_colorRenderbuffer);
            _colorRenderbuffer = 0;
        }
    }
}

- (void) setFramebuffer
{
    if ([self setContext]) {
        if (!_defaultFramebuffer) {
            [self createFramebuffer];
        }
        glBindFramebuffer(GL_FRAMEBUFFER, _defaultFramebuffer);
    }
}

- (BOOL) presentFramebuffer
{
    BOOL success = FALSE;
    
    if ([self setContext]) {
        glBindRenderbuffer(GL_RENDERBUFFER, _colorRenderbuffer);
        
        success = [self.context presentRenderbuffer:GL_RENDERBUFFER];
    }
    return success;
}

- (void) presentTexture:(OffscreenFractal*)fractalRenderer isComplete:(BOOL)complete
{
    if (complete && _wantCapture) {
        _wantCapture = NO;

        // XXX Factor this mess out, for sanity.
        
        GLint backingWidth, backingHeight;

        // Bind the color renderbuffer used to render the OpenGL ES view
        // If your application only creates a single color renderbuffer which is already bound at this point,
        // this call is redundant, but it is needed if you're dealing with multiple renderbuffers.
        // Note, replace "_colorRenderbuffer" with the actual name of the renderbuffer object defined in your class.
        glBindRenderbuffer(GL_RENDERBUFFER, _colorRenderbuffer);

        // Get the size of the backing CAEAGLLayer
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);

        NSInteger x = 0, y = 0, width = backingWidth, height = backingHeight;
        NSInteger dataLength = width * height * 4;
        GLubyte *data = (GLubyte*)malloc(dataLength * sizeof(GLubyte));

        // Read pixel data from the framebuffer
        glPixelStorei(GL_PACK_ALIGNMENT, 4);
        glReadPixels(x, y, width, height, GL_RGBA, GL_UNSIGNED_BYTE, data);

        // Create a CGImage with the pixel data
        // If your OpenGL ES content is opaque, use kCGImageAlphaNoneSkipLast to ignore the alpha channel
        // otherwise, use kCGImageAlphaPremultipliedLast
        CGDataProviderRef ref = CGDataProviderCreateWithData(NULL, data, dataLength, NULL);
        CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();
        CGSize actualSize = [fractalRenderer getSize];
        CGImageRef iref = CGImageCreate(actualSize.width, actualSize.height,
                                        8, 32, width * 4, colorspace,
                                        kCGBitmapByteOrder32Big | kCGImageAlphaPremultipliedLast,
                                        ref, NULL, true, kCGRenderingIntentDefault);

        // OpenGL ES measures data in PIXELS
        // Create a graphics context with the target size measured in POINTS
        NSInteger widthInPoints, heightInPoints;
        if (NULL != UIGraphicsBeginImageContextWithOptions) {
            // On iOS 4 and later, use UIGraphicsBeginImageContextWithOptions to take the scale into consideration
            // Set the scale parameter to your OpenGL ES view's contentScaleFactor
            // so that you get a high-resolution snapshot when its value is greater than 1.0
            CGFloat scale = self.contentScaleFactor;
            widthInPoints = actualSize.width / scale;
            heightInPoints = actualSize.height / scale;
            UIGraphicsBeginImageContextWithOptions(CGSizeMake(widthInPoints, heightInPoints), NO, scale);
        }
        else {
            // On iOS prior to 4, fall back to use UIGraphicsBeginImageContext
            widthInPoints = actualSize.width;
            heightInPoints = actualSize.height;
            UIGraphicsBeginImageContext(CGSizeMake(widthInPoints, heightInPoints));
        }

        CGContextRef cgcontext = UIGraphicsGetCurrentContext();

        // UIKit coordinate system is upside down to GL/Quartz coordinate system
        // Flip the CGImage by rendering it to the flipped bitmap context
        // The size of the destination area is measured in POINTS
        CGContextSetBlendMode(cgcontext, kCGBlendModeCopy);
        CGContextDrawImage(cgcontext, CGRectMake(0.0, 0.0, widthInPoints, heightInPoints), iref);

        // Retrieve the UIImage from the current context
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        
        UIGraphicsEndImageContext();
        
        // Clean up
        free(data);
        CFRelease(ref);
        CFRelease(colorspace);
        CGImageRelease(iref);


        UIImageWriteToSavedPhotosAlbum (image, nil, nil, nil);
//        return image;
        
    }

    [self setFramebuffer];

    glViewport(0, 0, self.bounds.size.width*2.0f, self.bounds.size.height*2.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    glUseProgram(_program);
    
    glBindBuffer(GL_ARRAY_BUFFER, _vertex_bo);
    glVertexAttribPointer(_vertex_name, 2, GL_FLOAT, 0, 0, NULL);
    glEnableVertexAttribArray(_vertex_name);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, [fractalRenderer getTextureName]);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glUniform1i(_fractal_uniform, 0);
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _element_bo);
    glFlush(); // Insure offscreen rendering is finished before blitting.
    glDrawElements(GL_TRIANGLE_STRIP, 4, GL_UNSIGNED_SHORT, (void*)0 );
    [self presentFramebuffer];
}

@end
