/* 
 * Copyright (c) 2013, Charlie Reiman <tallest_head@nanomoai.com>
 * 
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
//
//  FractalGalleryViewController.m
//
//  Created by Charlie Reiman on 3/21/13.
//

#import "FractalGalleryViewController.h"
#import "FractalLibrary.h"
#import <QuartzCore/QuartzCore.h>

@interface FractalGalleryViewController ()

- (void) swipe:(UISwipeGestureRecognizer*)swipeGesture;
- (void) tap:(UITapGestureRecognizer*)tapGesture;

@end

@implementation FractalGalleryViewController

- (FractalGalleryViewController*) initWithAccessor:(id<GUIStateAccess>)accessor
{
    if ((self=[super initWithAccessor:accessor]) != nil) {
        UISwipeGestureRecognizer *swipeGesture;
        UITapGestureRecognizer *tapGesture;

        swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipe:)];
        swipeGesture.direction = UISwipeGestureRecognizerDirectionLeft;
        [self.view addGestureRecognizer:swipeGesture];

        swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipe:)];
        swipeGesture.direction = UISwipeGestureRecognizerDirectionRight;
        [self.view addGestureRecognizer:swipeGesture];

        tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)];
        [self.view addGestureRecognizer:tapGesture];
        
    }
    return self;
}

- (void) updateImage
{
    // This needs to be factored out as it is a copy of enshadow from
    // InteractGalleryVC.
    void (^enshadow)(UIView *) = ^(UIView *view) {
        view.layer.shadowColor = [UIColor blackColor].CGColor;
        view.layer.shadowOffset = CGSizeMake(2.0, 2.0);
        view.layer.shadowRadius = 2.0;
        view.layer.shadowOpacity = 0.5;
        UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:(CGRect){0, 0, 196, 196}];
        view.layer.shadowPath = shadowPath.CGPath;
    };


    [self.view.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    UIImage *galleryImage = [UIImage imageNamed:self.guiState.currentFractal.image_file];
    UIImageView *galleryImageView = [[UIImageView alloc] initWithImage:galleryImage];
    enshadow(galleryImageView);
    [self.view addSubview:galleryImageView];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor clearColor]];
	[self updateImage];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) swipe:(UISwipeGestureRecognizer*)swipeGesture
{
    if (swipeGesture.direction == UISwipeGestureRecognizerDirectionLeft) {
        [self.guiState nextFractal];
    }
    else if (swipeGesture.direction == UISwipeGestureRecognizerDirectionRight) {
        [self.guiState prevFractal];
    }
    [self updateImage];
}

- (void) tap:(UITapGestureRecognizer*)tapGesture
{
    CGPoint tapPoint = [tapGesture locationInView:self.view];
    float thirds = (3.0 * tapPoint.x) / self.view.bounds.size.width;

    if (thirds < 1.0) {
        // left side tap
        [self.guiState prevFractal];
        [self updateImage];
    }
    else if (thirds > 2.0) {
        // right side tap
        [self.guiState nextFractal];
        [self updateImage];
    }
    else {
        // center tap
    }
}


@end
