/* 
 * Copyright (c) 2013, Charlie Reiman <tallest_head@nanomoai.com>
 * 
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
//
//  FractalLibrary.m
//
//  Created by Charlie Reiman on 3/22/13.
//

#import "FractalLibrary.h"

typedef struct fractal_desc
{
    const char  *name;
    const char  *author;
    const char  *info;
    const char  *image_file;
    const char  *shader_file;
    const char  *default_config_file;
    const char  *user_config_file;
} fractal_desc;

static fractal_desc fractals[] = {
    {
        "Mandelbox",
        "Jan \"Řrřola\" Kadlec",
        "The mandelbox (AKA amazing box) was invented by Tom Lowe in 2010. Jan Kadlec's fragment shader made real time exploration "
        "of the fractal possible. Other fractals in Boxplorer are usually derivatives of Kadlec's work.",
        "fractals/mandelbox/gallery.jpg",
        "fractals/mandelbox/shader",
        "fractals/mandelbox/default.cfg",
        "fractals_mandelbox_user.cfg"
    },
    {
        "Pkleinian",
        "Knighty",
        "Theli-at posted the the MandelBulb3D code for the pkleinian on fractalforums in February of 2011. Knighty wrote the fragment shader version (as near as I can figure). I have not been able to contact knighty to get their real name.",
        "fractals/pkleinian/gallery.jpg",
        "fractals/pkleinian/shader",
        "fractals/pkleinian/default.cfg",
        "fractals_pkleinian_user.cfg"
    },
    {
        "Kalibox3d",
        "Tglad, Kali, Řrřola",
        "Kalibox is a modification of the standard mandelbox by Pablo Roman Andrioli (Kali). "
        "It replaces the double fold per axis with a single fold (abs). Compared to Mandelbox, it has less symmetry and cathedral-like interior spaces.",
        "fractals/kalibox3d/gallery.jpg",
        "fractals/kalibox3d/shader",
        "fractals/kalibox3d/default.cfg",
        "fractals_kalibox3d_user.cfg"
    },
    {
        "icosakifs",
        "Knighty, Bermarte, and Marius",
        "Icoaskifs is the Kadlec mandelbox shader modified to use icosahedral folding.",
        "fractals/icosakifs/gallery.jpg",
        "fractals/icosakifs/shader",
        "fractals/icosakifs/default.cfg",
        "fractals_icosakifs_user.cfg"
    },
#if 0
    {
        // Marius-3 just renders nothingness. I've spent enough time on
        // it for now.
        "marius-3",
        "",
        "Lorem ipsum",
        "fractals/marius-3/gallery.jpg",
        "fractals/marius-3/shader",
        "fractals/marius-3/default.cfg",
        "fractals_marius-3_user.cfg"
    },
    // Reflectoids is expensive and doesn't render reflections for some reason.
    // I can't figure out what's wrong so for now it just isn't shipping.
    {
        "Reflectoids",
        "knighty",
        "The pkleinian is something lorem ipsum",
        "fractals/reflectoids/gallery.jpg",
        "fractals/reflectoids/shader",
        "fractals/reflectoids/default.cfg",
        "fractals_reflectoids_user.cfg"
    },
#endif

};

@interface FractalDesc ()
{
    NSInteger _index;
}
@end

@implementation FractalDesc

- (FractalDesc*) initWithIndex:(NSInteger)index
{
    if ((self=[super init]) != nil) {
        _index = index;
    }
    return self;
}

- (NSInteger) index
{
    return _index;
}

#define FETCH(X) \
- (NSString*) X { \
    return [NSString stringWithCString:fractals[_index].X encoding:NSUTF8StringEncoding]; \
}

FETCH(name)
FETCH(author)
FETCH(info)
FETCH(image_file)
FETCH(shader_file)
FETCH(default_config_file)
FETCH(user_config_file)

@end

@implementation FractalLibrary

+ (FractalDesc*) getDesc:(NSInteger)index
{
    return [[FractalDesc alloc] initWithIndex:index];
}

+ (NSInteger) count
{
    return sizeof(fractals)/sizeof(fractals[0]);
}


@end
