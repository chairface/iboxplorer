/* 
 * Copyright (c) 2013, Charlie Reiman <tallest_head@nanomoai.com>
 * 
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
//
//  GranularityView.m
//
//  Created by Charlie Reiman on 4/15/13.
//

#import "GranularityView.h"
#import <QuartzCore/QuartzCore.h>


@interface GranularityView ()
{
    float _max;
    float _min;
    float _cooked;
}

@property (nonatomic, strong) CALayer *thumbLayer;

@end


@implementation GranularityView

- (void) layoutSublayersOfLayer:(CALayer *)layer
{
    CAShapeLayer *scaleLayer = [CAShapeLayer layer];
    layer.backgroundColor = [UIColor clearColor].CGColor;
    
    CGMutablePathRef scalePath = CGPathCreateMutable();
    CGPoint topLeft = CGPointMake(CGRectGetMinX(layer.bounds), CGRectGetMinY(layer.bounds));
    CGPoint botRight = CGPointMake(CGRectGetMaxX(layer.bounds), CGRectGetMaxY(layer.bounds));
    float  curLine = botRight.y;
    float  curFrac = 1.0;

    // bounding box
    CGPathAddRect(scalePath, nil, CGRectOffset(layer.bounds, 0.5, 0.5));
    
    // Scale lines.
    for (int i = 0; i<15; i++) { // 15 is arbitrary. We could make it smart but it isn't worth the time.
        float y = roundf(curLine)+0.5;
        CGPathMoveToPoint(scalePath, nil, topLeft.x, y);
        CGPathAddLineToPoint(scalePath, nil, botRight.x, y);
        curFrac *= (2.0/3.0);
        curLine = curFrac * (layer.bounds.size.height) + topLeft.y;
    }

    // Center line
    //CGPathMoveToPoint(scalePath, nil, CGRectGetMidX(layer.bounds), topLeft.y);
    //CGPathAddLineToPoint(scalePath, nil, CGRectGetMidX(layer.bounds), botRight.y);

    // Shadow layer
    CGAffineTransform moveUp = CGAffineTransformMakeTranslation(0, 1);
    CGPathRef shadowPath = CGPathCreateCopyByTransformingPath(scalePath, &moveUp);
    CAShapeLayer *shadowLayer = [CAShapeLayer layer];
    shadowLayer.path = shadowPath;
    shadowLayer.backgroundColor = [UIColor clearColor].CGColor;
    shadowLayer.strokeColor = [UIColor blackColor].CGColor;
    shadowLayer.fillColor = [UIColor grayColor].CGColor;
    shadowLayer.opacity = 0.8;
    
    [layer addSublayer:shadowLayer];
    CGPathRelease(shadowPath);

    // Finally, make that shape layer
    scaleLayer.path = scalePath;
    scaleLayer.strokeColor = [UIColor whiteColor].CGColor;
    scaleLayer.fillColor = [UIColor clearColor].CGColor;

    [layer addSublayer:scaleLayer];
    CGPathRelease(scalePath);

    // Thumb goes on top, so add it after.
    CALayer *thumb = [CALayer layer];
    thumb.backgroundColor = [UIColor yellowColor].CGColor;
    thumb.bounds = CGRectMake(0.0, 0.0, layer.bounds.size.width+4, 5.0);
    [layer addSublayer:thumb];

    // Min, max, and granularity are the exponent (base 10) of cookedGranularity
    _min = -3.0;
    _max = 0.0;
    self.thumbLayer = thumb;
    self.granularity = _max;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    [self handleTouch:touch];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    [self handleTouch:touch];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
}

- (void) handleTouch:(UITouch*)touch
{
    CGPoint gPoint = [touch locationInView:self];
    
    self.granularity = _min + ((gPoint.y / self.bounds.size.height)*(_max-_min));
}

- (void) setGranularity:(float)granularity
{
    if (granularity < _min) granularity = _min;
    if (granularity > _max) granularity = _max;
    _granularity = granularity;
    float height_fract = (granularity-_min)/(_max-_min);
    CGPoint newPos;
    newPos.x = self.bounds.size.width*0.5;
    newPos.y = self.bounds.size.height * height_fract;
    [CATransaction setDisableActions:YES];
    self.thumbLayer.position = newPos;
    _cooked = powf(10.0, _granularity);
}

- (float) cookedGranularity
{
    return _cooked;
}

@end
