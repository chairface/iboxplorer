/* 
 * Copyright (c) 2013, Charlie Reiman <tallest_head@nanomoai.com>
 * 
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
//
//  InfoViewController.m
//
//  Created by Charlie Reiman on 3/21/13.
//

#import "InfoExploreViewController.h"
#import "DrawDriver.h"
#import "AppEvents.h"
#import "UIButton+Pretty.h"

@interface InfoExploreViewController ()

@end

@implementation InfoExploreViewController

- (InfoExploreViewController*) initWithAccessor:(id<GUIStateAccess>)accessor
{
    if ((self=[super initWithAccessor:accessor]) != nil) {
        // There's no view yet so there isn't much you can do here.
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    if ([UIScreen mainScreen].scale == 1.0) {
        [self.segmentControl setEnabled:NO forSegmentAtIndex:3];
    }
    self.segmentControl.selectedSegmentIndex = 0; // selecting in the XIB causes weirdness
    [self.captureButton prettyUp];
}

- (void) loadView
{
    NSArray *xibArray = [[NSBundle mainBundle] loadNibNamed:@"InfoExploreView" owner:self options:nil];
    self.view = xibArray[0];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - User actions
- (IBAction) doResize:(UISegmentedControl*)sender
{
    NSUInteger size = 128;

    size <<= [sender selectedSegmentIndex];
    [self.delegate infoExploreViewController:self setSize:(float)size];
    [[DrawDriver instance] setCtlChange];
}

- (IBAction) doProgressive:(UISwitch*)sender
{
    [self.delegate infoExploreViewController:self setProgressive:[sender isOn]];
}

- (IBAction) doCapture:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kAppEventCapture object:self];
    [[DrawDriver instance] scheduleDraw]; // Force an extra blit since we probably aren't actively rendering.
}

@end

