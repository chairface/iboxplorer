/* 
 * Copyright (c) 2013, Charlie Reiman <tallest_head@nanomoai.com>
 * 
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
//
//  IntView.m
//
//  Created by Charlie Reiman on 4/6/13.
//

#import "IntView.h"
#import "DrawDriver.h"

@implementation IntView
{
    UISlider *_slider;
    UILabel  *_valueLabel;
}

- (id) initWithFrame:(CGRect)rowFrame color:(UIColor*)rowColor value:(CfgValue*)cVal
{
    self = [super initWithFrame:rowFrame];
    if (self) {
        float rowHeight = rowFrame.size.height;
        float width = rowFrame.size.width;

        self.backgroundColor = rowColor;
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(4, 0, (width/2)-4, rowHeight/3)];
        label.text = cVal.name;
        label.backgroundColor = [UIColor clearColor];
        [self addSubview:label];

        _slider = [[UISlider alloc] initWithFrame:CGRectMake(4, rowHeight/3, width-8, rowHeight/3)];
        _slider.maximumValue = cVal.intMaxValue;
        _slider.minimumValue = cVal.intMinValue;
        _slider.value = cVal.intValue;
        
        [_slider addTarget:self action:@selector(setIntValue:) forControlEvents:UIControlEventValueChanged];
        [self addSubview:_slider];
        _valueLabel = [[UILabel alloc] initWithFrame:CGRectMake((width/2)+4, 0, (width/2)-8, rowHeight/3)];
        _valueLabel.text = [NSString stringWithFormat:@"%d", cVal.intValue];
        _valueLabel.textAlignment = NSTextAlignmentRight;
        _valueLabel.backgroundColor = [UIColor clearColor];
        [self addSubview:_valueLabel];

        self.cVal = cVal;
    }
    return self;
}

- (void) setIntValue:(UISlider*)control
{
    float oldVal = self.cVal.intValue;
    float newVal = roundf(control.value);

    control.value = newVal;

    if (oldVal != newVal) {
        _valueLabel.text = [NSString stringWithFormat:@"%d", (int)newVal];
        self.cVal.intValue = (int)newVal;
        [[DrawDriver instance] setCtlChange];
    }
}

- (void) setCVal:(CfgValue *)cVal
{
    [super setCVal:cVal];
    _slider.value = cVal.intValue;
    _valueLabel.text = [NSString stringWithFormat:@"%d", cVal.intValue];
}

@end
