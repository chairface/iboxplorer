/* 
 * Copyright (c) 2013, Charlie Reiman <tallest_head@nanomoai.com>
 * 
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
//
//  InteractExploreViewController.h
//
//  Created by Charlie Reiman on 3/25/13.
//

#import "SubViewController.h"
#import "ThrottleView.h"
#import "TranslateView.h"
#import "FOVView.h"
#import "GranularityView.h"

@interface InteractExploreViewController : SubViewController <FOVViewDelegate>

@property (nonatomic, weak) id<ControlProtocol> delegate;
@property (nonatomic) float scale;  // Control scale, does not apply to FOV
@property (nonatomic, strong) IBOutlet ThrottleView *throttle;
@property (nonatomic, strong) IBOutlet TranslateView *translate;
@property (nonatomic, strong) IBOutlet FOVView *fovView;
@property (nonatomic, strong) IBOutlet GranularityView *granularity;
@property (nonatomic, strong) IBOutlet UIButton *doneButton;
@property (nonatomic, strong) IBOutlet UIButton *moreButton;

- (IBAction) doDone:(id)sender;
- (IBAction) doMore:(id)sender;

@end
