/* 
 * Copyright (c) 2013, Charlie Reiman <tallest_head@nanomoai.com>
 * 
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
//
//  InteractExploreViewController.m
//
//  Created by Charlie Reiman on 3/25/13.
//

#import "InteractExploreViewController.h"
#import "ControllingView.h"
#import "DrawDriver.h"
#import "InfoExploreViewController.h"
#import "AppEvents.h"
#import "UIButton+Pretty.h"

@interface InteractExploreViewController ()

@end

@implementation InteractExploreViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) viewDidLoad
{
    [super viewDidLoad];
	for (ControllingView *subview in self.view.subviews) {
        if ([subview isKindOfClass:[ControllingView class]]) {
            [subview setControlDelegate:self.delegate];
        }
    }
    self.fovView.delegate = self;
    [self.moreButton prettyUp];
    [self.doneButton prettyUp];
}

- (void) loadView
{
    NSArray *xibArray = [[NSBundle mainBundle] loadNibNamed:@"InteractExploreView" owner:self options:nil];
    self.view = xibArray[0];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction) doDone:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kAppEventSaveConfig object:self];
    [[NSNotificationCenter defaultCenter] postNotificationName:kAppEventExploreDone object:self];
}

- (void) doMore:(NSNotification*)notice
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kAppEventEditParameters object:self];
}

#pragma mark - ControlProtocol

- (void) fovView:(FOVView *)fovview fov:(CGPoint)fov
{
    [self.delegate setFOVWidth:fov.x];
    [self.delegate setFOVHeight:fov.y];
    [[DrawDriver instance] setCtlChange];
}


@end
