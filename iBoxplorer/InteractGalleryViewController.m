/* 
 * Copyright (c) 2013, Charlie Reiman <tallest_head@nanomoai.com>
 * 
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
//
//  InteractionViewController.m
//
//  Created by Charlie Reiman on 3/21/13.
//

#import "InteractGalleryViewController.h"
#import "FractalLibrary.h"
#import <QuartzCore/QuartzCore.h>
#import "UIButton+Pretty.h"

@interface InteractGalleryViewController ()

@end

// Looking for gallery image tap? That's in the root view controller as it
// spans view controllers. It might have been a mistake though.

@implementation InteractGalleryViewController

- (InteractGalleryViewController*) initWithAccessor:(id<GUIStateAccess>)accessor
{
    if ((self=[super initWithAccessor:accessor]) != nil) {
    }
    return self;
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    [self loadForCurrentFractal];
    [self.exploreButton prettyUp];
    self.view.backgroundColor = [UIColor clearColor]; // colorWithPatternImage:[UIImage imageNamed:@"pleather.png"]];
    self.view.layer.backgroundColor = [UIColor clearColor].CGColor;
}

- (void) loadView
{
    NSArray *xibArray = [[NSBundle mainBundle] loadNibNamed:@"InteractGalleryView" owner:self options:nil];
    self.view = xibArray[0];
}


- (void) loadForCurrentFractal
{
    FractalDesc *fractalInfo = [self.guiState currentFractal];
    self.nameLabel.text = [fractalInfo name];
    self.authorLabel.text = [fractalInfo author];
    self.infoText.text = [fractalInfo info];
    
    NSString *galleryFileName = [fractalInfo image_file];
    NSArray *galleryFileNameBits = [galleryFileName componentsSeparatedByString:@"."];
    NSString *galleryFormat = [galleryFileNameBits[0] stringByAppendingString:@"%d.jpg"];

    void (^enshadow)(UIView *) = ^(UIView *view) {
        view.layer.shadowColor = [UIColor blackColor].CGColor;
        view.layer.shadowOffset = CGSizeMake(3.0, 3.0);
        view.layer.shadowRadius = 2.0;
        view.layer.shadowOpacity = 0.5;
        UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:view.bounds];
        view.layer.shadowPath = shadowPath.CGPath;
    };

    UIImageView *(^getImage)(int num, int width) = ^UIImageView*(int num, int width) {
        NSString *imageFilename = [NSString stringWithFormat:galleryFormat, num];
        UIImage *imageObj = [UIImage imageNamed:imageFilename];
        UIImageView *result = [[UIImageView alloc] init];
        result.frame = CGRectMake(0, 0, width, width);
        result.image = imageObj;
        enshadow(result);
        return result;
    };

    for (UIView *gallery in @[self.gallery1, self.gallery2, self.gallery3, self.gallery4]) {
        for (UIView *subview in gallery.subviews) {
            [subview removeFromSuperview];
        }
    }
    [self.gallery1 addSubview:getImage(1, self.gallery1.bounds.size.width)];
    [self.gallery2 addSubview:getImage(2, self.gallery2.bounds.size.width)];
    [self.gallery3 addSubview:getImage(3, self.gallery3.bounds.size.width)];
    [self.gallery4 addSubview:getImage(4, self.gallery4.bounds.size.width)];
}

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction) doExplore:(UIButton*)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kIIVCExploreButton object:self];
}

@end


