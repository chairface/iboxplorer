/* 
 * Copyright (c) 2013, Charlie Reiman <tallest_head@nanomoai.com>
 * 
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
//
//  OffscreenFractal.h
//
//  Created by Charlie Reiman on 3/30/13.
//

#import <Foundation/Foundation.h>
#import "ControlProtocol.h"
#import "Config.h"

#define kNewFractalFrame @"com.nanomoai.iboxplorer.newframe"

@protocol GUIStateAccess;

@interface OffscreenFractal : NSObject <ControlProtocol>

@property (nonatomic, strong) id<GUIStateAccess>  guiState;
@property (nonatomic, strong) Config *config;

- (OffscreenFractal*) init;
- (Config*) readConfig;
- (void) createFramebuffer;
- (void) bindFrameBuffer;
- (void) prepareQuad;
- (void) loadShaders;
- (void) setUniforms;
- (void) releaseTexture;
- (void) resizeTexture:(CGSize)newSize;
- (void) setProgressive:(BOOL)progressive;
- (void) drawTexture:(id)isCtrls;
- (GLuint) getTextureName;
- (CGPoint) getFOV;
- (CGSize) getSize;

@end
