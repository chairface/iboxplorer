/* 
 * Copyright (c) 2013, Charlie Reiman <tallest_head@nanomoai.com>
 * 
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
//
//  OffscreenFractal.m
//
//  Created by Charlie Reiman on 3/30/13.
//

// This class is responsible for rendering the fractal into an offscreen
// texture. I'm designing this to be fine grained so callers are going to
// be responsible for insuring correct behavior. Likewise, I'm going to
// try and make most methods idempotent so I can change the rendering
// parameters at well.

#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/ES2/glext.h>

#import "OffscreenFractal.h"
#import "GLARE.h"
#import "Camera.h"
#import "GUIStateAccess.h"
#import "FractalLibrary.h"
#import "Shader.h"
#import "CfgValue.h"
#import "DrawDriver.h"
#import "AppEvents.h"
#import <math.h>

// These are the partial rendering squares constants. These are N nested squares
// where the the square areas are 1, 2, 3...N. So rendering each subsequent square
// adds the same amout of area.
// For four squares, which wasn't enough:
//#define A 0.5               /* sqrt(1)/2 */
//#define B 0.70710678118655  /* sqrt(2)/2 or A*sqrt(2) */
//#define C 0.86602540378444  /* sqrt(3)/2 or A*sqrt(3) */
//#define D 1.0               /* sqrt(4)/2 or A*sqrt(4) */

#define A 0.40824829046386
#define B 0.57735026918962
#define C 0.70710678118654
#define D 0.81649658092772
#define E 0.91287092917527
#define F 1.0

static const GLfloat squareVertices[] =
{ //   0         1         2         3
    A, A,    A, -A,   -A, -A,    -A, A,  // 0
    B, B,    B, -B,   -B, -B,    -B, B,  // 4
    C, C,    C, -C,   -C, -C,    -C, C,  // 8
    D, D,    D, -D,   -D, -D,    -D, D,  // 12
    E, E,    E, -E,   -E, -E,    -E, E,  // 16
    F, F,    F, -F,   -F, -F,    -F, F,  // 20
};

// The elements are laid out so each of the four steps can be drawn
// independently or as one continous array.
static const GLushort squareElements[] = {
     1,  2,  0,  3,                             // Innermost square - an actual square
     4,  4,  0,  5,  1,  6,  2,  7,  3,  4,  0, // outer squares have a square hole in them.
     8,  8,  4,  9,  5, 10,  6, 11,  7,  8,  4,
    12, 12,  8, 13,  9, 14, 10, 15, 11, 12,  8,
    16, 16, 12, 17, 13, 18, 14, 19, 15, 16, 12,
    20, 20, 16, 21, 17, 22, 18, 23, 19, 20, 16,
    16, 16, // worried about stepping off the end of the array
};

// I may be confused on how glDrawElements is supposed to count things.
#define DOUGHNUT_LEN  12
#define DOUGHNUT_1    ((void*)8)
#define DOUGHNUT_2    ((void*)30)
#define DOUGHNUT_3    ((void*)52)
#define DOUGHNUT_4    ((void*)74)
#define DOUGHNUT_5    ((void*)96)

@interface OffscreenFractal ()
{
    GLuint _fboName;
    GLuint _textureName;
    GLuint _program;

    float  _fovX;
    float  _fovY;
    CGSize _renderSize;
    GLint  _rwidth; // texture Size (power of two)
    GLint  _rheight;
    Camera *_camera;

    // Our quad gl names
    GLuint  _vertex_bo;
    GLuint  _element_bo;
    
    // Fractal specific uniforms
    GLint  *_uniforms;
    // Universal uniforms
    GLint  _uniformFovX;
    GLint  _uniformFovY;
    GLint  _uniformCamera;
    // Vertex attribute
    GLuint _vertex_location;

    float velX, velY, velZ;  // Current velocity

    BOOL  _progressive;
    NSInteger _progStep;
}


@end

@implementation OffscreenFractal

- (OffscreenFractal*) init
{
    self = [super init];
    if (self) {
        _camera = [[Camera alloc] init];
        _progressive = YES;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resetConfig:) name:kAppEventResetConfig object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(saveConfig:) name:kAppEventSaveConfig object:nil];
    }
    return self;
}

- (void) createFramebuffer
{
    glGenFramebuffers(1, &_fboName); GLARE;
}

- (void) bindFrameBuffer
{
    glBindFramebuffer(GL_FRAMEBUFFER, _fboName);
}

- (void) releaseFrameBuffer
{
    if (_fboName != 0 ) {
        glDeleteFramebuffers(1, &_fboName); GLARE;
        _fboName = 0;
    }
}

// Be sure to set the guistate delegate and config IVs before calling!
- (void) loadShaders
{
    NSAssert(_config != nil, @"Config not set");
    NSAssert(_guiState != nil, @"GUI state not set");
    
    // XXX Add some error checking. It's unlikely to fail but better check anyway.
    // Load the shaders
    NSString *bundlePath = [[NSBundle mainBundle] bundlePath];
    NSString *shaderPath = [bundlePath stringByAppendingPathComponent:[self.guiState.currentFractal shader_file]];
    NSString *fragShaderPath = [shaderPath stringByAppendingString:@".fsh"];
    NSString *vertShaderPath = [shaderPath stringByAppendingString:@".vsh"];
    NSString *fragFile = [NSString stringWithContentsOfFile:fragShaderPath encoding:NSASCIIStringEncoding error:nil];
    NSString *vertFile = [NSString stringWithContentsOfFile:vertShaderPath encoding:NSASCIIStringEncoding error:nil];
    Shader *shaderLoader = [[Shader alloc] initWithShadersFrag:fragFile vert:vertFile];

    _program = [shaderLoader programObject];
    glUseProgram(_program); GLARE;
    [shaderLoader validateProgram];

    // Get uniform locations.
    if (_uniforms) { free(_uniforms); }
    _uniforms = malloc(sizeof(GLint)*[self.config.values count]);
    int index = 0;
    for (CfgValue *cfgValue in self.config.values) {
        if (!cfgValue.hidden) {
            _uniforms[index] = [shaderLoader getUniformLocation:[cfgValue cName]];
            if (_uniforms[index] == -1) {
                NSLog(@"Ah crap. %@ not found.", cfgValue.name);
            }
        }
        index++;
    }
    _uniformCamera = [shaderLoader getUniformLocation:"camera"];
    _uniformFovX = [shaderLoader getUniformLocation:"fov_x"];
    _uniformFovY = [shaderLoader getUniformLocation:"fov_y"];
    _vertex_location = [shaderLoader getAttributeLocation:"vertex"];
}

- (void) setUniforms
{
    glUniformMatrix4fv(_uniformCamera, 1, NO, [_camera cameraUniform]);
    glUniform1f(_uniformFovX, _fovX); 
    glUniform1f(_uniformFovY, _fovY); 

    int index = 0;
    for (CfgValue *cfgValue in self.config.values) {
        if (!cfgValue.hidden) {
            if ([cfgValue.vtype isEqualToString:[CfgValue typeFloat]]) {
                glUniform1f(_uniforms[index], cfgValue.floatValue);
            }
            else if ([cfgValue.vtype isEqualToString:[CfgValue typeInt]]) {
                glUniform1i(_uniforms[index], cfgValue.intValue);
            }
            else if ([cfgValue.vtype isEqualToString:[CfgValue typeColor]]) {
                glUniform3fv(_uniforms[index], 1, [cfgValue colorValues]);
            }
        }
        index++;
    }

}

- (void) releaseTexture
{
    if (_textureName != 0) {
        glDeleteTextures(1, &_textureName);
        _textureName = 0;
    }
}

// This does gl binds so I think you need to have the FBO bound. I'll do it
// in here to be safe.
- (void) prepareQuad
{
    [self bindFrameBuffer];
    
    if (_vertex_bo != 0) {
        glDeleteBuffers(1, &_vertex_bo);
        _vertex_bo = 0;
    }
    if (_element_bo != 0) {
        glDeleteBuffers(1, &_element_bo);
        _element_bo = 0;
    }
    glGenBuffers(1, &_vertex_bo); GLARE;
    glBindBuffer(GL_ARRAY_BUFFER, _vertex_bo); GLARE;
    glBufferData(GL_ARRAY_BUFFER, sizeof(squareVertices), squareVertices, GL_STATIC_DRAW); GLARE;
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &_element_bo); GLARE;
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _element_bo); GLARE;
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(squareElements), squareElements, GL_STATIC_DRAW); GLARE;
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

// Resize the texture. This involves recreating it so it can also
// just create a new texture.
- (void) resizeTexture:(CGSize)newSize
{
    if (!CGSizeEqualToSize(newSize, _renderSize)) {
        [self bindFrameBuffer];
        [self releaseTexture];
        _renderSize = newSize;
        _rwidth = exp2(ceil(log2(_renderSize.width)));
        _rheight = exp2(ceil(log2(_renderSize.height)));

        // Attach a texture as the color buffer
        glGenTextures(1, &_textureName); GLARE;
        glBindTexture(GL_TEXTURE_2D, _textureName); GLARE;
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, _rwidth, _rheight, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL); GLARE;
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, _textureName, 0); GLARE;

        // And now set texture parameters
        glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR ); GLARE;
        glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR ); GLARE;

        GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER); GLARE;
        if(status != GL_FRAMEBUFFER_COMPLETE) {
            NSLog(@"%s:%d failed to make complete framebuffer object 0x%x", __func__, __LINE__, status);
        }
        glClearColor(0.0, 0.0, 0.0, 1.0);
        glClear(GL_COLOR_BUFFER_BIT);
    }
}

- (CGSize) getSize
{
    return _renderSize;
}

- (void) setProgressive:(BOOL)progressive
{
    _progressive = progressive;
    _progStep = 0;
}

// if isCtrls is not nil, this is a control change call
// and the progressive render should be reset.
- (void) drawTexture:(id)isCtrls
{
    if (isCtrls) {
        _progStep = 0;
    }
    // Apply camera movement
    [_camera moveCameraX:velX Y:velY Z:-velZ];

    // Bind the FBO and set coordinates
    [self bindFrameBuffer];
    glViewport(0, 0, _rwidth, _rheight);

    glUseProgram(_program);
    [self setUniforms];

    // Bind vertex buffer and pass in the verticies
    glBindBuffer(GL_ARRAY_BUFFER, _vertex_bo);
    glEnableVertexAttribArray(_vertex_location);
    glVertexAttribPointer(_vertex_location, 2, GL_FLOAT, GL_FALSE, 0, 0);

    // Bind the triangles
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _element_bo);

    // Finally see if we are ok and ready to go.
#if 0
    GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER) ;
    if(status != GL_FRAMEBUFFER_COMPLETE) {
        NSLog(@"%s:%d failed to make complete framebuffer object 0x%x", __func__, __LINE__, status);
    }
    else
#endif
    {
        // Finally get to the drawing.
        if (_progressive) {
            switch (_progStep) {
                case 0:
                    glDrawElements(GL_TRIANGLE_STRIP, 4, GL_UNSIGNED_SHORT, 0);
                    break;
                case 1:
                    glDrawElements(GL_TRIANGLE_STRIP, DOUGHNUT_LEN, GL_UNSIGNED_SHORT, DOUGHNUT_1);
                    break;
                case 2:
                    glDrawElements(GL_TRIANGLE_STRIP, DOUGHNUT_LEN, GL_UNSIGNED_SHORT, DOUGHNUT_2);
                    break;
                case 3:
                    glDrawElements(GL_TRIANGLE_STRIP, DOUGHNUT_LEN, GL_UNSIGNED_SHORT, DOUGHNUT_3);
                    break;
                case 4:
                    glDrawElements(GL_TRIANGLE_STRIP, DOUGHNUT_LEN, GL_UNSIGNED_SHORT, DOUGHNUT_4);
                    break;
                case 5:
                    glDrawElements(GL_TRIANGLE_STRIP, DOUGHNUT_LEN, GL_UNSIGNED_SHORT, DOUGHNUT_5);
                    break;
            }
            if (_progStep++ == 5) {
                _progStep = 0;
                // The object passed with the notice is 1 if this is a final frame, suitable for... framing.
                [[NSNotificationCenter defaultCenter] postNotificationName:kNewFractalFrame object:[NSNumber numberWithBool:YES]];
            }
            else {
                [[DrawDriver instance] scheduleDraw];
                [[NSNotificationCenter defaultCenter] postNotificationName:kNewFractalFrame object:[NSNumber numberWithBool:NO]];
            }
        }
        else {
            glClear(GL_COLOR_BUFFER_BIT);
            glDrawElements(GL_TRIANGLE_STRIP, (sizeof(squareElements)/sizeof(squareElements[0]))-2, GL_UNSIGNED_SHORT, 0);
            [[NSNotificationCenter defaultCenter] postNotificationName:kNewFractalFrame object:[NSNumber numberWithBool:YES]];
        }
    }
}

// Get the drawn texture out so it can be blitted on screen
- (GLuint) getTextureName
{
    return _textureName;
}


#pragma mark - ControlProtocol

// Linear, camera relative movement. X is right, y is up, Z is fwd.
- (void) translateX:(float)x Y:(float)y Z:(float)z
{
    // I still love my cubics!
    velX = x; // (x*x*x)/5.0;
    velY = y; // (y*y*y)/5.0;
    velZ = z; // (z*z*z)/5.0;
}

// Rotate the viewpoint.
//  X: pitch
//  Y: yaw
//  Z: roll
// These should all be in radians
- (void) orientX:(float)pitch Y:(float)yaw Z:(float)roll
{
    if (yaw != 0.0) {
        [_camera rotateCamera:yaw X:1.0 Y:0.0 Z:0.0];
    }
    if (pitch != 0.0) {
        [_camera rotateCamera:pitch X:0.0 Y:1.0 Z:0.0];
    }
    if (roll != 0.0) {
        [_camera rotateCamera:roll X:0.0 Y:0.0 Z:1.0];
    }
}

// Set the texture size
- (void) setWidth:(float)x height:(float)y
{
    [self resizeTexture:CGSizeMake(x, y)];
}

- (void) setFOVWidth:(float)angle
{
    _fovX = angle;
}

- (void) setFOVHeight:(float)angle
{
    _fovY = angle;
}

#pragma mark - etc

- (CGPoint) getFOV
{
    return CGPointMake(_fovX, _fovY);
}

- (Config*) readConfig
{
    FractalDesc *fracDesc = [self.guiState currentFractal];
    _config = [[Config alloc] initWithDefault:fracDesc.default_config_file User:fracDesc.user_config_file];
    [Config setGlobalConfig:_config];
    [_camera loadConfig:_config];
    return _config;
}

- (void) resetConfig:(NSNotification*)notice
{
    FractalDesc *fracDesc = [self.guiState currentFractal];
    [_config deleteUserData];
    
    _config = [[Config alloc] initWithDefault:fracDesc.default_config_file User:fracDesc.user_config_file];
    [Config setGlobalConfig:_config];
    [_camera loadConfig:_config];
    [[NSNotificationCenter defaultCenter] postNotificationName:kAppEventResetSync object:self];
}

- (void) saveConfig:(NSNotification*)notice
{
    GLfloat *cameraStuff = [_camera cameraUniform];
    BPvec3  cameraSet;
    CfgValue *vecToSet;

    // Up
    cameraSet.x = cameraStuff[4];
    cameraSet.y = cameraStuff[5];
    cameraSet.z = cameraStuff[6];
    vecToSet = [_config valueForKey:@"upDirection"];
    [vecToSet setVec3Value:cameraSet];
    
    // Direction
    cameraSet.x = cameraStuff[8];
    cameraSet.y = cameraStuff[9];
    cameraSet.z = cameraStuff[10];
    vecToSet = [_config valueForKey:@"direction"];
    [vecToSet setVec3Value:cameraSet];

    // Position
    cameraSet.x = cameraStuff[12];
    cameraSet.y = cameraStuff[13];
    cameraSet.z = cameraStuff[14];
    vecToSet = [_config valueForKey:@"position"];
    [vecToSet setVec3Value:cameraSet];

    [_config writeToUserPath];
}

- (void) dealloc
{
    [self releaseTexture];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kAppEventResetConfig object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kAppEventSaveConfig object:nil];
    if (_uniforms) { free(_uniforms); }
}


@end
