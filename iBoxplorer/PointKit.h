// PointKit
// Some handy extensions to the CoreGraphics CGPoint type
// Charlie Reiman, 2013.05.16

// This file is in the public domain.

#ifndef _pointkit_h_
#define _pointkit_h_

#include <math.h>

#if CGFLOAT_IS_DOUBLE
#define PKSQRT sqrt
#define PKATAN2 atan2
#define PKCOS cos
#define PKSIN sin
#else
#define PKSQRT sqrtf
#define PKATAN2 atan2f
#define PKCOS cosf
#define PKSIN sinf
#endif


static inline CGPoint PKAdd(CGPoint a, CGPoint b) {
    return (CGPoint){a.x+b.x, a.y+b.y};
}

static inline CGPoint PKSubtract(CGPoint a, CGPoint b) {
    return (CGPoint){a.x-b.x, a.y-b.y};
}

static inline CGPoint PKNormal(CGPoint a) {
    return (CGPoint){a.y, -a.x};
}

static inline CGPoint PKMultiply(CGPoint a, CGFloat s) {
    return (CGPoint){a.x*s, a.y*s};
}

static inline CGFloat PKDot(CGPoint a, CGPoint b) {
    return a.x*b.x + a.y*b.y;
}

static inline CGFloat PKMagnitude(CGPoint a) {
    return PKSQRT(a.x*a.x+a.y*a.y);
}

static inline CGFloat PKAngle(CGPoint a) {
    return PKATAN2(a.y, a.x);
}

static inline CGPoint PKNormalize(CGPoint a) {
    CGFloat mag = PKMagnitude(a);
    if (mag == 0.0) return CGPointZero;
    else return PKMultiply(a, ((CGFloat)1.0)/mag);
}

static inline CGFloat PKDistance(CGPoint a, CGPoint b) {
    return PKMagnitude(PKSubtract(a, b));
}

static inline CGPoint PKCMultiply(CGPoint a, CGPoint b) {
    return (CGPoint){(a.x*b.x)-(a.y*b.y),(a.x*b.y)+(a.y*b.x)};
}

static inline CGPoint PKRotate(CGPoint a, CGFloat theta) {
    CGFloat c=PKCOS(theta);
    CGFloat s=PKSIN(theta);
    return (CGPoint){a.x*c - a.y*s, a.x*s + a.y*c};
}

#undef PKSQRT
#undef PKATAN2
#undef PKCOS
#undef PKSIN

#endif
