/* 
 * Copyright (c) 2013, Charlie Reiman <tallest_head@nanomoai.com>
 * 
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
//
//  RootViewController.m
//
//  Created by Charlie Reiman on 3/21/13.
//

#import "RootViewController.h"
#import "FractalLibrary.h"

// Primary, gallery view controllers
#import "InfoGalleryViewController.h"
#import "InteractGalleryViewController.h"
#import "FractalGalleryViewController.h"

// Exploration view controllers
#import "InfoExploreViewController.h"
#import "InteractExploreViewController.h"
#import "ParametersViewController.h"
#import "FractalExploreViewController.h"
#import "FractalGLView.h"

#import "AppEvents.h"
#import "PointKit.h"

#import "UIButton+Pretty.h"

#import <QuartzCore/QuartzCore.h>

#define kAnimationDuration (0.5)

@interface RootViewController ()
{
    NSInteger _fractalIndex;
    SubViewController *_savedExploreVC;
    UIView *_modalPopup;
    BOOL _animatingGallerySwap;
}

+ (UIImage*) backgroundImage;
- (void) setupGalleryAnimated:(BOOL)animate;
- (void) setupExploreAnimated:(BOOL)animate;

@end

@implementation RootViewController

+ (UIImage*) backgroundImage
{
    return [UIImage imageNamed:@"pleather.png"];
}

- (void) viewDidLoad
{
    [super viewDidLoad];

    _fractalIndex = 0;
    self.view.backgroundColor = [UIColor colorWithPatternImage:[RootViewController backgroundImage]];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doExplore:) name:kIIVCExploreButton object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doGallery:) name:kAppEventExploreDone object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doMore:) name:kAppEventEditParameters object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doParamDone:) name:kAppEventParameterDone object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doHelp:) name:kAppEventDoHelp object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doAbout:) name:kAppEventDoAbout object:nil];
    [self setupGalleryAnimated:NO];

    // Quick and dirty shadows for my 3 views
//    void (^enshadow)(UIView *) = ^(UIView *view) {
//        view.layer.shadowColor = [UIColor blackColor].CGColor;
//        view.layer.shadowOffset = CGSizeMake(2.0, 2.0);
//        view.layer.shadowRadius = 2.0;
//        view.layer.shadowOpacity = 0.5;
//        UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:view.bounds];
//        view.layer.shadowPath = shadowPath.CGPath;
//    };

//    for (UIView *view in @[self.fractalView, self.infoView, self.interactView]) {
//        enshadow(view);
//    }
    //enshadow(self.fractalView);
    _animatingGallerySwap = NO;
}

- (void) setupGalleryAnimated:(BOOL)animate
{
    void (^addGesture)(UIView *) = ^(UIView* view) {
        UITapGestureRecognizer *tapGest = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(galleryTap:)];
        [view addGestureRecognizer:tapGest];
        [view setUserInteractionEnabled:YES]; // Just being paranoid
    };
    
    // Set up info
    self.infoVC = [[InfoGalleryViewController alloc] initWithAccessor:self];

    // Set up interaction
    InteractGalleryViewController *interVC = [[InteractGalleryViewController alloc] initWithAccessor:self];
    self.interVC = interVC;
    // Force load the view so we can get at the subviews. 
    [interVC view];
    // Attach tap gestures to the tiny gallery images
    for (UIView *tinyImage in @[interVC.gallery1, interVC.gallery2, interVC.gallery3, interVC.gallery4]) {
        addGesture(tinyImage);
    }
    
    // Set up fractal -- This one has no XIB
    self.fractalVC = [[FractalGalleryViewController alloc] initWithAccessor:self];
    self.fractalVC.view.frame = self.fractalView.bounds; // adjust to occupy entire parent view

    if (animate && [self.infoView.subviews count]>0) {
        [UIView transitionFromView:self.infoView.subviews[0] toView:self.infoVC.view duration:kAnimationDuration options:UIViewAnimationOptionTransitionFlipFromBottom completion:nil];
        [UIView transitionFromView:self.interactView.subviews[0] toView:self.interVC.view duration:kAnimationDuration options:UIViewAnimationOptionTransitionFlipFromRight completion:nil];
        [UIView transitionFromView:self.fractalView.subviews[0] toView:self.fractalVC.view duration:kAnimationDuration options:UIViewAnimationOptionTransitionFlipFromLeft completion:nil];

    }
    else {
        [self.infoView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [self.interactView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [self.fractalView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        
        [self.infoView addSubview:self.infoVC.view];
        [self.interactView addSubview:self.interVC.view];
        [self.fractalView addSubview:self.fractalVC.view];
    }
}

- (void) setupExploreAnimated:(BOOL)animate
{
    // Set up info
    InfoExploreViewController *infoExploreViewController;
    infoExploreViewController = [[InfoExploreViewController alloc] initWithAccessor:self];
    self.infoVC = infoExploreViewController;

    // Set up interaction
    InteractExploreViewController *interactExploreViewController;
    interactExploreViewController = [[InteractExploreViewController alloc] initWithAccessor:self];
    self.interVC = interactExploreViewController;

    // Set up fractal -- This one has no XIB
    FractalExploreViewController *fractalExploreViewController;
    FractalGLView *fractalView = [[FractalGLView alloc] initWithFrame:self.fractalView.bounds];
    fractalExploreViewController = [[FractalExploreViewController alloc] initWithAccessor:self];
    self.fractalVC = fractalExploreViewController;
    self.fractalVC.view = fractalView; // .frame = self.fractalView.bounds; // adjust to occupy entire parent view
    [self.fractalVC viewDidLoad];

    if (animate) {
        [UIView transitionFromView:self.infoView.subviews[0] toView:self.infoVC.view duration:kAnimationDuration options:UIViewAnimationOptionTransitionFlipFromTop completion:nil];
        [UIView transitionFromView:self.interactView.subviews[0] toView:self.interVC.view duration:kAnimationDuration options:UIViewAnimationOptionTransitionFlipFromLeft completion:nil];
        [UIView transitionFromView:self.fractalView.subviews[0] toView:self.fractalVC.view duration:kAnimationDuration options:UIViewAnimationOptionTransitionFlipFromRight completion:nil];
    }
    else {
        [self.infoView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [self.interactView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [self.fractalView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        
        [self.infoView addSubview:self.infoVC.view];
        [self.interactView addSubview:self.interVC.view];
        [self.fractalView addSubview:self.fractalVC.view];
    }
    interactExploreViewController.delegate = fractalExploreViewController.controllable;
    infoExploreViewController.delegate = fractalExploreViewController;
    // Well I guess my design isn't good enough so I'm going to just screw over my
    // encapsulation to get the velocity controls to work.
    fractalExploreViewController.throttle = interactExploreViewController.throttle;
    fractalExploreViewController.translate = interactExploreViewController.translate;
    fractalExploreViewController.granularity = interactExploreViewController.granularity;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) nextFractal
{
    _fractalIndex += 1;
    if (_fractalIndex == [FractalLibrary count]) {
        _fractalIndex = 0;
    }
    if ([self.interVC isKindOfClass:[InteractGalleryViewController class]]) {
        [(InteractGalleryViewController*)self.interVC loadForCurrentFractal];
    }
}

- (void) prevFractal
{
    _fractalIndex -= 1;
    if (_fractalIndex == -1) {
        _fractalIndex = [FractalLibrary count] - 1;
    }
    if ([self.interVC isKindOfClass:[InteractGalleryViewController class]]) {
        [(InteractGalleryViewController*)self.interVC loadForCurrentFractal];
    }
}

- (FractalDesc*) currentFractal
{
    return [FractalLibrary getDesc:_fractalIndex];
}

#pragma mark - Rotation (old)
- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIDeviceOrientationLandscapeRight) ||
            (interfaceOrientation == UIDeviceOrientationLandscapeLeft);
}

#pragma mark - User actions

- (void) doExplore:(NSNotification*)notice
{
    [self setupExploreAnimated:YES];
}

- (void) doGallery:(NSNotification*)notice
{
    //[self.fractalVC viewWillDisappear:NO]; // Apparently I have to call this myself...
    [self setupGalleryAnimated:YES];
}

- (void) doMore:(NSNotification*)notice
{
    // Save old view controller
    _savedExploreVC = self.interVC;
    // Create a new one
    self.interVC = [[ParametersViewController alloc] initWithAccessor:self];

    [UIView transitionFromView:self.interactView.subviews[0] toView:self.interVC.view duration:kAnimationDuration options:UIViewAnimationOptionTransitionFlipFromRight completion:nil];
}

- (void) doParamDone:(NSNotification*)notice
{
    self.interVC = _savedExploreVC;
    
    [UIView transitionFromView:self.interactView.subviews[0] toView:self.interVC.view duration:kAnimationDuration options:UIViewAnimationOptionTransitionFlipFromLeft completion:^(BOOL done) {
        _savedExploreVC = nil;
    }];

}

- (void) dismissDialog:(UIButton*)button
{
    if (_modalPopup) {
        [_modalPopup removeFromSuperview];
        _modalPopup = nil;
    }
}

#define TINY_IMAGE_TAG 22
#define BIG_IMAGE_TAG 33
#define TINY_CONTAINER_TAG 44
#define BIG_CONTAINER_TAG 55

// Someone tapped a litte gallery preview image. Swap it
// with the big gallery image
- (void) galleryTap:(UITapGestureRecognizer*)gesture
{
    const float animDuration = 0.5;
    if (!_animatingGallerySwap) {
        // Keep the UI from doing bad things while we animate.
        _animatingGallerySwap = YES;
        InteractGalleryViewController *intVC = (InteractGalleryViewController*)self.interVC;
        intVC.exploreButton.enabled = NO;

        UIView *bigContainer = self.fractalVC.view;
        UIView *tinyContainer = gesture.view;
        UIView *bigImage = [bigContainer subviews][0];
        UIView *tinyImage = [tinyContainer subviews][0];

        // Tag them so I can find them later.
        bigImage.tag = BIG_IMAGE_TAG;
        tinyImage.tag = TINY_IMAGE_TAG;
        bigContainer.tag = BIG_CONTAINER_TAG;
        tinyContainer.tag = TINY_CONTAINER_TAG;

        CGRect bigTargetFrame = [gesture.view convertRect:gesture.view.bounds toView:self.view];
        CGRect bigStartFrame = [self.view convertRect:bigImage.bounds fromView:bigImage];

        [self.view addSubview:bigImage];
        [self.view addSubview:tinyImage];
        bigImage.frame = bigStartFrame;
        tinyImage.frame = bigTargetFrame;
        
        // Prepare to swap the model settings AFTER the animations
        // are added to teh keyframe.

        // So remember positions...
        CGPoint oldBigImagePosition = bigImage.layer.position;
        CGPoint oldTinyImagePosition = tinyImage.layer.position;
        
        // And the bounds...
        CGRect oldBigBounds = bigImage.layer.bounds;
        CGRect oldTinyBounds = tinyImage.layer.bounds;

        // Then set up the crazy animations.
        UIBezierPath *smallPath = [UIBezierPath bezierPath];
        UIBezierPath *bigPath = [UIBezierPath bezierPath];

        [smallPath moveToPoint:tinyImage.center];
        [bigPath moveToPoint:bigImage.center];

        CGPoint midPoint = PKAdd(tinyImage.center, bigImage.center);
        midPoint = PKMultiply(midPoint, 0.5);

        float k = -0.8;
        // disp is the perpendicular vector
        CGPoint displ = PKNormal(PKSubtract(tinyImage.center, bigImage.center));
        CGPoint bigCtlPoint, littleCtlPoint;
        displ = PKMultiply(displ, k);
        
        bigCtlPoint = PKAdd(midPoint, displ);
        littleCtlPoint = PKSubtract(midPoint, displ);

        [smallPath addQuadCurveToPoint:bigImage.center controlPoint:littleCtlPoint];
        [bigPath addQuadCurveToPoint:tinyImage.center controlPoint:bigCtlPoint];

        CAKeyframeAnimation *tinyAnim = [CAKeyframeAnimation animationWithKeyPath:@"position"];
        tinyAnim.path = smallPath.CGPath;
        tinyAnim.duration = animDuration;
        tinyAnim.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        tinyAnim.calculationMode = kCAAnimationPaced;
        tinyAnim.delegate = tinyImage;

        CAKeyframeAnimation *bigAnim = [CAKeyframeAnimation animationWithKeyPath:@"position"];
        bigAnim.path = bigPath.CGPath;
        bigAnim.duration = animDuration;
        bigAnim.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        bigAnim.calculationMode = kCAAnimationPaced;
        bigAnim.delegate = tinyImage;

        CABasicAnimation *scaleUpAnim = [CABasicAnimation animation];
        scaleUpAnim.keyPath = @"bounds";
        scaleUpAnim.fromValue = [NSValue valueWithCGRect:oldTinyBounds];
        scaleUpAnim.toValue = [NSValue valueWithCGRect:oldBigBounds];
        scaleUpAnim.duration = animDuration;

        CABasicAnimation *scaleDownAnim = [CABasicAnimation animation];
        scaleUpAnim.keyPath = @"bounds";
        scaleDownAnim.fromValue = [NSValue valueWithCGRect:oldBigBounds];
        scaleDownAnim.toValue = [NSValue valueWithCGRect:oldTinyBounds];
        scaleDownAnim.duration = animDuration;

        scaleDownAnim.delegate = self; // Someone needs to tell me when we're done.
        
        [tinyImage.layer addAnimation:scaleUpAnim forKey:@"bounds"];
        [bigImage.layer addAnimation:scaleDownAnim forKey:@"bounds"];

        [tinyImage.layer addAnimation:tinyAnim forKey:@"position"];
        [bigImage.layer addAnimation:bigAnim forKey:@"position"];

        // Ok now we can swap the model. The animations are actually already in progress. Or not.
        // I think they wait till the runloop ticks. Whatever. This works.
        bigImage.layer.position = oldTinyImagePosition;
        tinyImage.layer.position = oldBigImagePosition;
        bigImage.layer.bounds = oldTinyBounds;
        tinyImage.layer.bounds = oldBigBounds;
    }
}

- (void) animationDidStop:(CAAnimation *)theAnimation finished:(BOOL)flag
{
    UIView *bigContainer = [self.view viewWithTag:BIG_CONTAINER_TAG];
    UIView *tinyContainer = [self.view viewWithTag:TINY_CONTAINER_TAG];
    UIView *bigImage = [self.view viewWithTag:BIG_IMAGE_TAG];
    UIView *tinyImage = [self.view viewWithTag:TINY_IMAGE_TAG];
    
    // Reenable UI
    _animatingGallerySwap = NO;

    InteractGalleryViewController *intVC = (InteractGalleryViewController*)self.interVC;
    intVC.exploreButton.enabled = YES;

    // Repair the view hierarchy
    [bigContainer addSubview:tinyImage];
    [tinyContainer addSubview:bigImage];
    bigImage.frame = tinyContainer.bounds;
    tinyImage.frame = bigContainer.bounds;

    // clear the tags
    bigContainer.tag = 0;
    tinyContainer.tag = 0;
    bigImage.tag = 0;
    tinyImage.tag = 0;
}


- (UIView*) webContentView:(NSString*)contents
{
    CGRect resultRect = CGRectInset(self.view.bounds, 200, 200);
    UIView *result = [[UIView alloc] initWithFrame:resultRect];
    result.backgroundColor = [UIColor lightGrayColor];
    result.layer.shadowColor = [UIColor blackColor].CGColor;
    result.layer.shadowOffset = CGSizeMake(2.0, 2.0);
    result.layer.shadowRadius = 3.0;
    result.layer.shadowOpacity = 1.0;
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:result.bounds];
    result.layer.shadowPath = shadowPath.CGPath;

    CGRect buttonRect = CGRectMake(20, result.bounds.size.height-60, result.bounds.size.width-40, 40);
    UIButton *dismissButton = [UIButton buttonWithType:UIButtonTypeCustom];
    dismissButton.frame = buttonRect;
    [dismissButton setTitle:@"Done" forState:UIControlStateNormal];
    [dismissButton prettyUp];
    [result addSubview:dismissButton];
    [dismissButton addTarget:self action:@selector(dismissDialog:) forControlEvents:UIControlEventTouchUpInside];

    CGRect webRect = CGRectMake(10, 10, result.bounds.size.width-20, result.bounds.size.height-80);
    UIWebView *webView = [[UIWebView alloc] initWithFrame:webRect];
    webView.delegate = self;
    [webView loadHTMLString:contents baseURL:[NSURL URLWithString:@"file:///"]];
    [result addSubview:webView];
    return result;
}

- (void) doAbout:(NSNotification*)notice
{
    NSError *error;

    NSURL *aboutURL = [[NSBundle mainBundle] URLForResource:@"about" withExtension:@"html"];
    NSString *aboutString = [NSString stringWithContentsOfURL:aboutURL encoding:NSASCIIStringEncoding error:&error];

    UIView *fadeView = [[UIView alloc] initWithFrame:self.view.bounds];
    fadeView.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.7];
    [fadeView addSubview:[self webContentView:aboutString]];
    [self.view addSubview:fadeView];
    _modalPopup = fadeView;
}

- (void) doHelp:(NSNotification*)notice
{
    NSError *error;

    NSURL *helpURL = [[NSBundle mainBundle] URLForResource:@"help" withExtension:@"html"];
    NSString *helpString = [NSString stringWithContentsOfURL:helpURL encoding:NSASCIIStringEncoding error:&error];

    UIView *fadeView = [[UIView alloc] initWithFrame:self.view.bounds];
    fadeView.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.7];
    [fadeView addSubview:[self webContentView:helpString]];
    [self.view addSubview:fadeView];
    _modalPopup = fadeView;
}

#pragma mark - WebView Delegate

-(BOOL)                       webView:(UIWebView *)inWeb
           shouldStartLoadWithRequest:(NSURLRequest *)inRequest
                       navigationType:(UIWebViewNavigationType)inType
{
    if ( inType == UIWebViewNavigationTypeLinkClicked ) {
        [[UIApplication sharedApplication] openURL:[inRequest URL]];
        return NO;
    }

    return YES;
}

@end

