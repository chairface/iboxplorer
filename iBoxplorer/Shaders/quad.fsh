precision mediump float;

uniform sampler2D fractal_texture;

varying vec2 tex_coor;

void main() {
    gl_FragColor = texture2D(fractal_texture, tex_coor);
}