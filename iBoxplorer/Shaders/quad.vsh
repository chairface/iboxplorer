
// Simple shader for blitting the full quad.
precision highp float;

attribute vec2 vertex;
varying vec2 tex_coor;

void main() {
    gl_Position = vec4(vertex, 0.0, 1.0);
    tex_coor = vec2(0.5,0.5) + (vertex * 0.5);
}
