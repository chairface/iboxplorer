/* 
 * Copyright (c) 2013, Charlie Reiman <tallest_head@nanomoai.com>
 * 
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
//
//  ThrottleView.m
//
//  Created by Charlie Reiman on 3/28/13.
//

#import "ThrottleView.h"
#import <QuartzCore/QuartzCore.h>

@interface ThrottleView ()

@property (nonatomic, strong) CALayer *thumbLayer;

- (void) handleTouch:(UITouch*)touch;

@end

static const uint8_t tickLen[]= {
    // Note: 0 and 16 are unused.
    0,   // 0000
    1,   // 0001
    2,   // 0010
    1,   // 0011
    3,   // 0100
    1,   // 0101
    2,   // 0110
    1,   // 0111
    4,   // 1000
    1,   // 1001
    2,   // 1010
    1,   // 1011
    3,   // 1100
    1,   // 1101
    2,   // 1110
    1,   // 1111
    0,   // 0000
};

    @implementation ThrottleView

// XXX Refactor this someplace useful
+ (CGMutablePathRef) createRoundRect:(CGRect)rect radius:(float)radius
{
    float ledge = rect.origin.x + 0.5;
    float redge = rect.origin.x + rect.size.width + 0.5;
    float tedge = rect.origin.y + 0.5;
    float bedge = rect.origin.y + rect.size.height + 0.5;
    
    CGMutablePathRef result = CGPathCreateMutable();
    CGPathMoveToPoint(result, nil, ledge+radius, tedge);
    CGPathAddArcToPoint(result, nil,
                        redge, tedge,
                        redge, tedge+radius,
                        radius);
    CGPathAddArcToPoint(result, nil,
                        redge, bedge,
                        redge-radius, bedge,
                        radius);
    CGPathAddArcToPoint(result, nil,
                        ledge, bedge,
                        ledge, bedge-radius,
                        radius);
    CGPathAddArcToPoint(result, nil,
                        ledge, tedge,
                        ledge+radius, tedge,
                        radius);
    return result;
}

- (void)layoutSublayersOfLayer:(CALayer *)layer
{
    layer.backgroundColor = [UIColor clearColor].CGColor;

    // Round rect and scale
    const float radius = 16;
    CGMutablePathRef layerPath = [ThrottleView createRoundRect:CGRectInset(self.bounds, 8, 0) radius:radius];
    CAShapeLayer *scaleLayer = [CAShapeLayer layer];

    // Round rect part

    // Scale part
    CGPoint scale0, scale1;
    scale0 = CGPointMake(0.5+CGRectGetMidX(layer.bounds), CGRectGetMinY(layer.bounds)+radius);
    scale1 = CGPointMake(0.5+CGRectGetMidX(layer.bounds), CGRectGetMaxY(layer.bounds)-radius);
    // Draw center line
    CGPathMoveToPoint(layerPath, nil, scale0.x, scale0.y);
    CGPathAddLineToPoint(layerPath, nil, scale1.x, scale1.y);
    // Now draw ticks
    float basicTickLen = layer.bounds.size.width*0.08333333333333;
    for (int i=1; i<=15; i++) {
        float j = (float)i;
        const float sixteenth = 1.0/16.0;
        CGPoint centerPoint;
        centerPoint.x = scale0.x;
        centerPoint.y = 0.5+roundf((j*sixteenth)*(scale1.y-scale0.y)+scale0.y);  // rounding gives more reliable results
        
        CGPathMoveToPoint(layerPath, nil, centerPoint.x-(basicTickLen*tickLen[i]), centerPoint.y);
        CGPathAddLineToPoint(layerPath, nil, centerPoint.x+(basicTickLen*tickLen[i]), centerPoint.y);
    }

    // shadow layer
    CGAffineTransform moveUp = CGAffineTransformMakeTranslation(0, 1);
    CGPathRef shadowPath = CGPathCreateCopyByTransformingPath(layerPath, &moveUp);
    CAShapeLayer *shadowLayer = [CAShapeLayer layer];
    shadowLayer.path = shadowPath;
    shadowLayer.backgroundColor = [UIColor clearColor].CGColor;
    shadowLayer.strokeColor = [UIColor blackColor].CGColor;
    shadowLayer.fillColor = [UIColor grayColor].CGColor;
    shadowLayer.opacity = 0.8;
    
    [layer addSublayer:shadowLayer];
    CGPathRelease(shadowPath);

    scaleLayer.path = layerPath;
    scaleLayer.strokeColor = [UIColor whiteColor].CGColor;
    scaleLayer.fillColor = [UIColor clearColor].CGColor;

    [layer addSublayer:scaleLayer];
    CGPathRelease(layerPath);
    
    // Thumb
    CALayer *thumb = [CALayer layer];
    thumb.backgroundColor = [UIColor yellowColor].CGColor;
    thumb.bounds = CGRectMake(0.0, 0.0, layer.bounds.size.width+4, 5.0);
    [layer addSublayer:thumb];

    // init layer
    self.thumbLayer = thumb;
    self.throttle = 0.0;
}

// There isn't a gesture recognizer that does what I want. Pan is close
// but no cigar. Doing it the old way is easy so that's what I'll do.

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    [self handleTouch:touch];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    self.throttle = 0.0;
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    [self handleTouch:touch];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    self.throttle = 0.0;
}

- (void) handleTouch:(UITouch*)touch
{
    CGPoint tPoint = [touch locationInView:self];
    self.throttle = 2*(tPoint.y / self.bounds.size.height)-1.0;
}

- (void) setThrottle:(float)throttle
{
    if (throttle>1.0) throttle = 1.0;
    if (throttle<-1.0) throttle = -1.0;
    _throttle = throttle;
    CGPoint newPos;
    float height2 = self.bounds.size.height * 0.5;
    newPos.x = self.bounds.size.width*0.5;
    newPos.y = (height2 * throttle) + height2;
    [CATransaction setDisableActions:YES];
    self.thumbLayer.position = newPos;
    [self.delegate throttleView:self setValue:throttle];
}

@end
