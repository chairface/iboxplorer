/* 
 * Copyright (c) 2013, Charlie Reiman <tallest_head@nanomoai.com>
 * 
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
//
//  TranslateView.m
//
//  Created by Charlie Reiman on 3/28/13.
//

#import "TranslateView.h"
#import <QuartzCore/QuartzCore.h>

@interface TranslateView ()

@property (nonatomic, strong) CALayer *thumbLayer;

- (void) handleTouch:(UITouch*)touch;

@end

const static float ctlMax = 0.6;
const static float ctlMin = -ctlMax;

@implementation TranslateView

- (void)layoutSublayersOfLayer:(CALayer *)layer
{
    // Rejigger our coordinate space so y goes up.
    CATransform3D transform = CATransform3DScale(CATransform3DIdentity, 1.0, -1.0, 1.0);
    // transform = CATransform3DTranslate(transform, layer.bounds.size.width*0.5, layer.bounds.size.height*0.5, 0);
    layer.transform = transform;

    layer.backgroundColor = nil;
    CGMutablePathRef bounds = CGPathCreateMutable();
    CGPathAddRect(bounds, nil, CGRectOffset(layer.bounds, 0.5, 0.5));
    CAShapeLayer *pathLayer = [CAShapeLayer layer];
    pathLayer.path = bounds;
    pathLayer.fillColor = nil;
    pathLayer.strokeColor = [UIColor whiteColor].CGColor;

    CGAffineTransform moveUp = CGAffineTransformMakeTranslation(0, -1);
    CGPathRef boundsShadow = CGPathCreateCopyByTransformingPath(bounds, &moveUp);
    CAShapeLayer *shadowLayer = [CAShapeLayer layer];
    shadowLayer.path = boundsShadow;
    shadowLayer.fillColor = [UIColor grayColor].CGColor;
    shadowLayer.strokeColor = [UIColor blackColor].CGColor;
    shadowLayer.opacity = 0.8;

    [layer addSublayer:shadowLayer];
    CGPathRelease(boundsShadow);

    [layer addSublayer:pathLayer];
    CGPathRelease(bounds);
    
    CGMutablePathRef circle = CGPathCreateMutable();
    CGPathAddEllipseInRect(circle, nil, CGRectMake(-5.0, -5.0, 10.0, 10.0));
    CAShapeLayer *circleLayer = [[CAShapeLayer alloc] init];
    circleLayer.path = circle;
    CGPathRelease(circle);
    circleLayer.lineWidth = 0.0;
    circleLayer.fillColor = [UIColor yellowColor].CGColor;
    self.thumbLayer = circleLayer;
    [layer addSublayer:circleLayer];

    self.motionVector = CGPointZero;
}

// There isn't a gesture recognizer that does what I want. Pan is close
// but no cigar. Doing it the old way is easy so that's what I'll do.

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    [self handleTouch:touch];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    self.motionVector = CGPointZero;
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    [self handleTouch:touch];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    self.motionVector = CGPointZero;
}

// Convert touch point to -1 to 1 range.
- (void) handleTouch:(UITouch*)touch
{
    CGPoint tPoint = [touch locationInView:self];
    tPoint.x = (ctlMax-ctlMin)*(tPoint.x/self.bounds.size.width)+ctlMin;
    tPoint.y = (ctlMax-ctlMin)*(tPoint.y/self.bounds.size.height)+ctlMin;
    self.motionVector = tPoint;
}

- (void) setMotionVector:(CGPoint)motionVector
{
    if (motionVector.x>ctlMax) motionVector.x = ctlMax;
    if (motionVector.x<ctlMin) motionVector.x = ctlMin;
    if (motionVector.y>ctlMax) motionVector.y = ctlMax;
    if (motionVector.y<ctlMin) motionVector.y = ctlMin;
    _motionVector = motionVector;

    float height2 = self.bounds.size.height * 0.5;
    float width2 = self.bounds.size.width * 0.5;
    CGPoint newPos;
    newPos.x = (width2 * (motionVector.x/ctlMax)) + width2;
    newPos.y = (height2 * (motionVector.y/ctlMax)) + height2;
    [CATransaction setDisableActions:YES];
    self.thumbLayer.position = newPos;
    [self.delegate translateView:self translateX:motionVector.x Y:motionVector.y];
}

@end
