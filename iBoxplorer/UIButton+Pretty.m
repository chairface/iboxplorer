/* 
 * Copyright (c) 2013, Charlie Reiman <tallest_head@nanomoai.com>
 * 
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
//
//  UIButton+Pretty.m
//
//  Created by Charlie Reiman on 5/16/13.
//
// See: http://mobile.tutsplus.com/tutorials/iphone/custom-uibutton_iphone/
//

#import "UIButton+Pretty.h"
#import <QuartzCore/QuartzCore.h>

@implementation UIButton (Pretty)

static const CGFloat C=1.0/255.0;

- (void) prettyUp
{
    UIButton *btn = self;
    
    btn.titleLabel.font = [UIFont boldSystemFontOfSize:16.0f];

    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateHighlighted];

    // Draw a custom gradient
    CAGradientLayer *btnGradient = [CAGradientLayer layer];
    btnGradient.frame = btn.bounds;
    [btn.layer insertSublayer:btnGradient atIndex:0];
    [btn setUnpressedGradient:btn];

    // Rounded corners
    CALayer *btnLayer = [btn layer];
    [btnLayer setMasksToBounds:YES];
    [btnLayer setCornerRadius:7.0f];

    // Apply a 1 pixel, black border
    [btnLayer setBorderWidth:1.0f];
    [btnLayer setBorderColor:[[UIColor grayColor] CGColor]];

    [btn addTarget:self action:@selector(setPressedGradient:) forControlEvents:UIControlEventTouchDown|UIControlEventTouchDragInside];
    [btn addTarget:self action:@selector(setUnpressedGradient:) forControlEvents:UIControlEventTouchUpInside|UIControlEventTouchDragOutside];
}


-(void)setPressedGradient:(id)sender
{
    UIButton *btnPressed = (UIButton *)sender;
    CAGradientLayer *btnLayer = [[btnPressed.layer sublayers] objectAtIndex:0];

    [CATransaction begin];
    [CATransaction setAnimationDuration:0.1];
    btnLayer.colors = @[(id)[UIColor colorWithRed:225.0f*C green:225.0f*C blue:225.0f*C alpha:1.0f].CGColor,
                        (id)[UIColor colorWithRed:130.0f*C green:130.0f*C blue:130.0f*C alpha:1.0f].CGColor];
    [CATransaction commit];
}

-(void)setUnpressedGradient:(id)sender
{
    UIButton *btnUnpressed = (UIButton *)sender;
    CAGradientLayer *btnLayer = [[btnUnpressed.layer sublayers] objectAtIndex:0];

    [CATransaction begin];
    [CATransaction setAnimationDuration:0.1];
    btnLayer.colors = @[(id)[UIColor colorWithRed:102.0f*C green:102.0f*C blue:102.0f*C alpha:1.0f].CGColor,
                        (id)[UIColor colorWithRed:51.0f*C green:51.0f*C blue:51.0f*C alpha:1.0f].CGColor];
    [CATransaction commit];
}
@end

