// Mandelbox shader by Rrrola
// Original formula by Tglad
// - http://www.fractalforums.com/3d-fractal-generation/amazing-fractal

precision highp float;

#define P0 p0                    // standard Mandelbox
//#define P0 vec4(par[1].x,par[1].y,par[2].y,1)  // Mandelbox Julia

uniform float s_term;
uniform float r2_term;

#define DIST_MULTIPLIER 1.0
#define MAX_DIST 4.0

#define BIGBIG 3.4e38

// Camera position and direction.
varying vec3 eye, dir;

uniform float min_dist,           // Distance at which raymarching stops.
    ao_eps,             // Base distance at which ambient occlusion is estimated.
    ao_strength,        // Strength of ambient occlusion.
    glow_strength,      // How much glow is applied after max_steps.
    dist_to_color;      // How is background mixed with the surface color after max_steps.

uniform int fractal_iterations,    // Number of fractal iterations.
    color_iterations,        // Number of fractal iterations for coloring.
    max_steps;          // Maximum raymarching steps.

// Colors. Can be negative or >1 for interestiong effects.
uniform vec3 surfaceColor1;
uniform vec3 surfaceColor2;
uniform vec3 surfaceColor3;
uniform vec3 glowColor;
uniform vec3 backgroundColor;
uniform vec3 specularColor;
uniform vec3 aoColor;

// precomputed constants
float minRad2 = clamp(r2_term, 1.0e-9, 1.0);
vec4 scale = vec4(s_term, s_term, s_term, abs(s_term)) / minRad2;
float absScalem1 = abs(s_term - 1.0);
float AbsScaleRaisedTo1mIters = pow(abs(s_term), float(1-fractal_iterations));

// Compute the distance from `pos` to the Mandelbox.
float d(vec3 pos) {
    vec4 p = vec4(pos,1), p0 = p;  // p.w is the distance estimate

    for (int i=0; i<fractal_iterations; i++) {
        // box folding: if (p>1) p = 2-p; else if (p<-1) p = -2-p;
        p.xyz = clamp(p.xyz, -1.0, 1.0) * 2.0 - p.xyz;  // min;max;mad

        // sphere folding: if (r2 < minRad2) p /= minRad2; else if (r2 < 1.0) p /= r2;
        float r2 = dot(p.xyz, p.xyz);
        p *= clamp(max(minRad2/r2, minRad2), 0.0, 1.0);  // dp3,div,max.sat,mul

        // scale, translate
        p = p*scale + P0;
    }
    return ((length(p.xyz) - absScalem1) / p.w - AbsScaleRaisedTo1mIters) * DIST_MULTIPLIER;
}


// Compute the color at `pos`.
vec3 color(vec3 pos) {
    vec3 p = pos, p0 = p;
    float trap = 1.0;

    for (int i=0; i<color_iterations; i++) {
        p.xyz = clamp(p.xyz, -1.0, 1.0) * 2.0 - p.xyz;
        float r2 = dot(p.xyz, p.xyz);
        p *= clamp(max(minRad2/r2, minRad2), 0.0, 1.0);
        p = p*scale.xyz + P0.xyz;
        trap = min(trap, r2);
    }
    // c.x: log final distance (fractional iteration count)
    // c.y: spherical orbit trap at (0,0,0)
    vec2 c = clamp(vec2( 0.33*log(dot(p,p))-1.0, sqrt(trap) ), 0.0, 1.0);

    return mix(mix(surfaceColor1, surfaceColor2, c.y), surfaceColor3, c.x);
}


float normal_eps = 0.00001;

// Compute the normal at `pos`.
// `d_pos` is the previously computed distance at `pos` (for forward differences).
vec3 normal(vec3 pos, float d_pos) {
    vec4 Eps = vec4(0, normal_eps, 2.0*normal_eps, 3.0*normal_eps);
    return normalize(vec3(
    // 2-tap forward differences, error = O(eps)
    //    -d_pos+d(pos+Eps.yxx),
    //    -d_pos+d(pos+Eps.xyx),
    //    -d_pos+d(pos+Eps.xxy)

    // 3-tap central differences, error = O(eps^2)
        -d(pos-Eps.yxx)+d(pos+Eps.yxx),
        -d(pos-Eps.xyx)+d(pos+Eps.xyx),
        -d(pos-Eps.xxy)+d(pos+Eps.xxy)

    // 4-tap forward differences, error = O(eps^3)
    //    -2.0*d(pos-Eps.yxx)-3.0*d_pos+6.0*d(pos+Eps.yxx)-d(pos+Eps.zxx),
    //    -2.0*d(pos-Eps.xyx)-3.0*d_pos+6.0*d(pos+Eps.xyx)-d(pos+Eps.xzx),
    //    -2.0*d(pos-Eps.xxy)-3.0*d_pos+6.0*d(pos+Eps.xxy)-d(pos+Eps.xxz)

    // 5-tap central differences, error = O(eps^4)
    //    d(pos-Eps.zxx)-8.0*d(pos-Eps.yxx)+8.0*d(pos+Eps.yxx)-d(pos+Eps.zxx),
    //    d(pos-Eps.xzx)-8.0*d(pos-Eps.xyx)+8.0*d(pos+Eps.xyx)-d(pos+Eps.xzx),
    //    d(pos-Eps.xxz)-8.0*d(pos-Eps.xxy)+8.0*d(pos+Eps.xxy)-d(pos+Eps.xxz)
    ));
}


// Blinn-Phong shading model with rim lighting (diffuse light bleeding to the other side).
// `normal`, `view` and `light` should be normalized.
vec3 blinn_phong(vec3 normal, vec3 view, vec3 light, vec3 diffuseColor) {
    vec3 halfLV = normalize(light + view);
    float spe = pow(max( dot(normal, halfLV), 0.0 ), 32.0);
    float dif = dot(normal, light) * 0.5 + 0.75;
    return dif*diffuseColor + spe*specularColor;
}


// Ambient occlusion approximation.
float ambient_occlusion(vec3 p, vec3 n) {
    float ao = 1.0, w = ao_strength/ao_eps;
    float dist = 2.0 * ao_eps;

    for (int i=0; i<5; i++) {
        float D = d(p + n*dist);
        ao -= (dist-D) * w;
        w *= 0.5;
        dist = dist*2.0 - ao_eps;  // 2,3,5,9,17
    }
    return clamp(ao, 0.0, 1.0);
}

void main() {
    vec3 p = eye, dp = normalize(dir);

    float totalD = 0.0, D = BIGBIG, extraD = 0.0, lastD;

    // Intersect the view ray with the Mandelbox using raymarching.
    int steps;
    for (steps=0; steps<max_steps; steps++) {
        lastD = D;
        D = d(p + totalD * dp);

        // Overstepping: have we jumped too far? Cancel last step.
        if (extraD > 0.0 && D < extraD) {
            totalD -= extraD;
            extraD = 0.0;
            D = BIGBIG;
            steps--;
            continue;
        }

        if (D < min_dist || D > MAX_DIST) break;

        totalD += D;

        // Overstepping is based on the optimal length of the last step.
        totalD += extraD = 0.096 * D*(D+extraD)/lastD;
    }

    p += totalD * dp;

    // Color the surface with Blinn-Phong shading, ambient occlusion and glow.
    vec3 col = backgroundColor;

    // We've got a hit or we're not sure.
    if (D < MAX_DIST) {
        vec3 n = normal(p, D);
        col = color(p);
        col = blinn_phong(n, -dp, normalize(eye+vec3(0,1,0)+dp), col);
        col = mix(aoColor, col, ambient_occlusion(p, n));

        // We've gone through all steps, but we haven't hit anything.
        // Mix in the background color.
        if (D > min_dist) {
            col = mix(col, backgroundColor, clamp(log(D/min_dist) * dist_to_color, 0.0, 1.0));
        }
    }

    // Glow is based on the number of steps.
    col = mix(col, glowColor, float(steps)/float(max_steps) * glow_strength);

    gl_FragColor = vec4(col, 1);
}
