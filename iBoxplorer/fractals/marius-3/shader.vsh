
uniform float fov_x, fov_y;  // Field of vision.
uniform mat4 camera;

attribute vec2 vertex;

varying vec3 eye, dir;

float fov2scale(float fov) { return tan(fov/2.0); }  // dropped radians() call -- cjr

// Draw an untransformed rectangle covering the whole screen.
// Get camera position and interpolated directions from the modelview matrix.
void main() {
    gl_Position = vec4(vertex, 0.0, 1.0);
    eye = vec3(camera[3]);
    dir = vec3(camera * vec4(fov2scale(fov_x)*vertex.x, fov2scale(fov_y)*vertex.y, 1, 0) );
}
